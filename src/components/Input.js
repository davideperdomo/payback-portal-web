import styled from 'styled-components';

const StyledInput = styled.input`
  font-family: Poppins;
  background: #FFFFFF;
  border: ${ props => props.warning ? '1px solid red' : '1px solid #DDE1EA'};
  border-radius: 4.03px;
  height: 28.8px;
  margin-bottom: 0.5em;
  &:focus {
    border: 1.5px solid #E8EAED;
  }
`

export default StyledInput;
