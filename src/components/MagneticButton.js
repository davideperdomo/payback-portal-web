import React, { useState, useEffect, useRef } from 'react';
import styled from 'styled-components';
import { Cursor } from '../assets/images/index';
import { Tween } from 'react-gsap';

export default function MagneticsButton(props){
  const [mousePos, setMousePos] = useState({left: 0, top: 0});
  const [buttonBounds, setButtonBounds] =  useState({left: 0, top: 0, width:0, height:0});
  const [magnetic, setMagnetic] = useState(null);

  const ref = useRef(null);

  const getMove = ( ba, ma) => {
    const move = magnetic ? (ma-ba)*0.6 : 0;//magnetic ? (ma-A/2) / A *max : 0;//
    return move;
  };

	const isMagnetic = ( bounds, x, y) => {
		const centerX = bounds.left + (bounds.width / 2)
		const centerY = bounds.top + (bounds.height / 2)

		const dx = Math.abs(centerX - x)
		const dy = Math.abs(centerY - y)
    //const c = Math.sqrt(dx*dx + dy*dy)
    const a = bounds.width * 1;
    const b = 42 * 2;
    const isInEllipse = ( (dx*dx)/(a*a) ) + ((dy*dy)/(b*b) ) <=1 ;
    //const isInCircle = c < (bounds.width / 2) + getThreshold(); 
		return isInEllipse;
	}
  
  useEffect( () => {
    const handleMove = (e) => {
      setMousePos({left: e.pageX, top: e.pageY});
      setMagnetic(isMagnetic(buttonBounds, e.pageX, e.pageY));
    };
    window.addEventListener('mousemove', handleMove);
    return () => window.removeEventListener('mousemove', handleMove);
  }, [mousePos, buttonBounds]);

  useEffect(() => {
    const position = ref.current.getBoundingClientRect();
    setButtonBounds(position);
  }, []);

  return (
    <Tween 
      to={{ 
        x: `${getMove( buttonBounds.left+(buttonBounds.width/2), mousePos.left )}px`,
        y: `${getMove( buttonBounds.top+6, mousePos.top)}px` 
      }} 
      duration={2} 
    >
      <Button onClick={props.onPress} magnetic={magnetic} >
        <p ref={ref}>{props.children}</p>
      </Button>
    </Tween>
  );
};

const Button = styled.button`
  background-color: ${props => props.magnetic ? '#45a5a5' : '#00BBBB' };
  border-radius: 5px;
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 12px;
  color: #FFFFFF;
  letter-spacing: 0.5px;
  text-align: center;
  height: 3em;
  border: none;
  min-width: 230px;
  line-height: 0px;
  cursor: url(${Cursor}) 12 12 , auto; 
`;
