import React, { useRef } from 'react';
import styled from 'styled-components';
import { PAGE2 } from '../assets/images-new'
import { Reveal, Tween } from 'react-gsap';

export default function TileList(props){
  const mainref = useRef(null);
  const ref1 = useRef(null);
  const ref2 = useRef(null);
  const ref3 = useRef(null);

  const getRef = (idx) => {
    if ( idx === 0) return ref1;
    if ( idx === 1 ) return ref2;
    return ref3;
  };

  return (
    <div>
    <Reveal repeat trigger={<div />}>
      <TileContainer className="tile" ref={mainref}>
        <Tween
          from={{ opacity: 0, transform: 'translate3d(0, 500px, 0)' }}
          duration={2}
          stagger={{ amount: 0.3, grid: [1, 3], axis: "x" }}
          ease="circ.out"
        >
        {props.data ? props.data.map((item, idx)=>{
          return (
            <Tile key={`tile${idx}`}>
              <Span icon={getIcon(item.spanName)} ></Span>
              <p ref={getRef(idx)}>
                <span>{item.title}</span>
                {item.content}</p>
            </Tile>
          );
        }) : "nodata"
        }
        </Tween>
      </TileContainer>
    </Reveal>
    </div>
  )
}

const getIcon = (iconname) => {
  if (iconname === "download") return PAGE2.IconBox1;
  if (iconname === "lightning") return PAGE2.IconBox2;
  if (iconname === "link") return PAGE2.IconBox3;
};

const Span = styled.span`
  background: url(${props => props.icon});
  width: 63px;
  height: 63px;
  margin: auto;
  margin-top: 2em;
  margin-bottom: 10px;
  background-size: contain;
  background-repeat: no-repeat;
  display: block;
  @media (max-width: 840px/*720px*/) {
    width: 50px;
    margin: 10px;
    align-text: left;
    grid-column: 1;
  };
`;

const TileContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 50px;
  margin: auto;

  @media (max-width: 840px/*720px*/) {
    width: 90vw;
    display: grid;
    margin: 0;
  };
`

const Tile = styled.div`
  background: #FFFFFF;
  margin: 1em;
  width: 239px;
  padding: 1em;

  background: #FFFFFF;
  box-shadow: 54px 54px 108px 0 rgba(0,0,0,0.15);
  border-radius: 15.12px;

  h3 {
    color: #FF4759;
  };
  p {
    font-size: 16px;
    color: #303643;
  };
  @media (max-width: 881px) and (min-width: 720px) {
    display: grid;
    grid-template-columns: 30% 70%;
    width: 90%;
    margin: 1em 0.5em;
    height: 90px;
    box-shadow: none;
    p {
      text-align: left;
      font-size: 0.8em;
    };
  };
  @media (max-width: 720px) {
    display: grid;
    grid-template-columns: 30% 70%;
    width: 90%;
    margin: 1em 0.5em;
    height: 90px;
    box-shadow: none;
    p {
      text-align: left;
      font-size: 0.8em;
    };
  };
`;
