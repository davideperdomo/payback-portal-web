import React, {useState} from 'react';
import { OTHERS } from '../assets/images-new'
import styled from 'styled-components';

const CardComponent = (props) => {
  const [toggle, setToggle] = useState(false);
  const onPress = () => setToggle(!toggle);
  const { item, idx } = props;

  return (
    <Card key={idx}>
      <h3>{item.title}</h3>
      { toggle ?
        <div className="text">
          <p>{item.content}</p>
          {item.url? <a href={item.url}>{item.url}</a> : <></>}
        </div>
        : <></>
      }
      <OpenDetail onClick={onPress}>
        <p>{toggle?"Cerrar":"Leer mas.."}</p>
        <Span
          icon={toggle?OTHERS.IconArrowUp:OTHERS.IconArrowDown}
          onClick={onPress}>
        </Span>
      </OpenDetail>
    </Card>
  );
}

export default function CardList(props){
  return (
    <CardContainer>
      {props.data.map((item, idx)=>{
        return (
          <div key={idx}>
            <CardComponent key={idx} idx={idx} item={item}/>
          </div>
        )
      })}
    </CardContainer>
  )
};

const OpenDetail = styled.div`
  display: grid;
  grid-template-columns: 95% 10%;
  border-top: 2px solid #F0F0F0;

  p {
    font-size: 0.8em;
    color: #3D3F2C;
    letter-spacing: 0;
    line-height: 14.64px;
  };
  @media (max-width: 720px) {
    grid-template-columns: 90% 10%;
  }
`;

const Span = styled.span`
  background: url(${props => props.icon});
  grid-column: 2;
  grid-row: 1;
  float: right;
  width: 19px;
  height: 12px;
  margin: 18px 10px 10px 10px;
  background-size: contain;
  background-repeat: no-repeat;
  display: block;
`;

const CardContainer = styled.div`
  margin: auto;
  text-align: left;
  max-width: 820px;
  margin-bottom: 60px;
  @media (max-width: 720px) {
    margin: 0.2em;
    margin-bottom: 60px;
  };
`;

const Card = styled.div`
  text-align: left;
  padding: 1em 1.4em;
  margin: 2em;

  box-shadow: 0 13px 24px 0 rgba(18,62,119,0.32);
  border-radius: 7.23px;
  border: 0 solid #F0F0F0;

  h3 {
    grid-column: 1;
    grid-row: 1;

    font-size: 0.8em;
    color: #1E1E1E;
    letter-spacing: 0;
    line-height: 13.12px;

    @media (max-width: 720px) {
      //font-size: 13px;
      letter-spacing: 0.46px;
      line-height: 1.6em;
    };
  };
  .text{
    font-weight: normal;
    font-size: 0.8em;
    color: #303643;
    letter-spacing: 0;
    line-height: 24px;
    @media (max-width: 720px) {
      font-size: 0.7em;
      letter-spacing: 0.2px;
      line-height: 18px;
    };
  };
  a {
    font-size: 14px;
    color: #303643;
    @media (max-width: 720px) {
      font-size: 12px;
    };
  };
  @media (max-width: 720px) {
    margin: 1em 0.8em;
  };
`;
