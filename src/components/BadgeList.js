import React, {useState} from 'react';
import Badge from "./Badge";
import styled from 'styled-components';
import { OTHERS } from '../assets/images-new'

const BadgeComponent = (props) => {
  const { item, idx, toggle, start } = props.settings;
  const onPress = props.onPress;
  return (
    <div>
      { (start) ?
        <Badge isactive={(toggle === idx)} onClick={() => onPress(idx)}>
          {item.title}
        </Badge> :
        <Badge isactive={(toggle === idx)} onClick={() => onPress(idx)}>
          {item.title}
        </Badge>
      }
      {(toggle === idx) ?
        <MessageBckg>
          <MsgArrow />
          <Message>
            <CloseICon onClick={() => onPress(null)}>
              <img src={OTHERS.IconClose} alt="close"/>
            </CloseICon>
            <div>
              <h3 id="item-title">{item.title}</h3>
              {item.content}
            </div>
          </Message>
        </MessageBckg>:
        <></>}
    </div>
  );
};

export default function BadgeList(props){
  const [toggle, setToggle] = useState(null);
  const [start, setStart] = useState(false);
  const onPress = (idx) => {
    if (!start) setStart(true);
    setToggle((idx === toggle)?null:idx);
  }
  return (
    <BadgeContainer >
        {props.data.map((item, idx)=>{
          return (
          <BadgeComponent
            key={`badge${idx}`}
            settings={{ item, idx, toggle, start }}
            onPress={onPress} />)
        })}
    </BadgeContainer>
  )
};

const Message = styled.div`
  position: absolute;
  background: #FFFFFF;
  box-shadow:0 9px 20px 7px rgba(0,0,0,0.50);
  border-radius: 8px;
  opacity: 0.96;
  font-weight: normal;
  font-size: 11px;
  letter-spacing: 0;
  line-height: 16px;
  max-width: 300px;
  min-width: 100px;
  padding: 1em 1em;
  text-align: left;
  color: #010101;
  z-index: 2;
  margin-top: 8px;
  #item-title {
    display: none;
  }
  @media (max-width: 720px) {
    margin: auto;
    position: relative;
    margin-top: 15em;
    padding: 2em;
    #item-title {
      display: block;
      margin-top: -4px;
    };
  };
`;
const MessageBckg = styled.div`
  @media (max-width: 720px) {
    background-color: rgb(0,0,0,0.5);
    position: fixed;
    width: 100vw;
    height: 100vh;
    right: 0;
    top: 0;
  };
`;

const CloseICon = styled.span`
  display: none;
  @media (max-width: 720px) {
    display: block;
    margin-left: 96%;
    width: 18px;
    margin-top: -10px;
  };
`;

const MsgArrow = styled.div`
  position: absolute;
  width: 1px;
  border: solid 10px transparent;
  border-bottom-color: rgb(255, 255, 255, 1);
  margin: -11px 0px 0 79px;
  z-index: 4;
  margin-right: auto;
  @media (max-width: 720px) {
    display: none;
  };
`;

const BadgeContainer = styled.div`
  padding: 0.5em 10em;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  @media (max-width: 720px) {
    padding: 0.5em 0.5em;
  };
`;
