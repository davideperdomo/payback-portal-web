import React from 'react';
import styled from 'styled-components';

export default function Button(props){

  return (
    <StyledButton props>
      {props.children}
    </StyledButton>
  )
};

const StyledButton = styled.button`
  font-family: 'Poppins';
  font-weight: 600;
  height: 3em;
  border: none;
  min-width: 230px;

  background: #FF4759;
  border-radius: 3.6px;
  font-size: 9px;
  color: #FFFFFF;
  letter-spacing: -0.4px;
  text-align: center;
`
