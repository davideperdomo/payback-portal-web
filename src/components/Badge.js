import styled from "styled-components";

const Badge = styled.div`
  background: #FFFFFF;
  box-shadow: 0 9px 14px -6px rgba(0,0,0,0.50);
  border-radius: 15px;
  font-weight: normal;
  font-size: 12px;
  color: #303643;
  line-height: 20px;
  padding: 0.5em 1.5em;
  margin: 0.5em;
  border: ${props => props.isactive? "2px solid #FFFFF": "1px solid rgba(0,0,0,0.10)" };
  width: fit-content;
  &:hover {
    border: 2px solid #303643;
  }
`;

export default Badge;