import React from 'react';
import { Reveal, Tween } from 'react-gsap';

export const FadeFromBottom = ({ children }) => (
  <Reveal repeat trigger={<div />}> 
    <Tween
      from={{ opacity: 0, transform: 'translate3d(0, 60vw, 0)' }}
      duration={2}
      ease="circ.out"
    >
      {children}
    </Tween>
  </Reveal>
);
