import React from 'react';
import styled from 'styled-components';
import { OTHERS } from '../assets/images-new'

export default function StoreButtons(props){
  return (
    <StoreContainer>
      <div
        className="google"
        onClick={props.onPressGoogle}>
        <img src={OTHERS.IconGstore} alt="play-store-link" />
      </div>
      <div
        className="ios"
        onClick={props.onPressIos}>
        <img src={OTHERS.IconIOSstore} alt="app-store-link" />
      </div>
    </StoreContainer>
  )
};

const StoreContainer = styled.div`
  display: flex;
  max-width: 230px;
  div {
    cursor: pointer;
  };
  @media (max-width: 720px) {
    margin: auto;
    text-align: center;
  };
  img {
    height: 34px;
  };
  .google {
    margin-right: 15px;
  };
`;
