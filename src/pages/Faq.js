import React from 'react';
import { useHistory } from "react-router-dom";
import { CardList, BadgeList } from '../components';
import { Content, Footer  } from '../containers';
import { OTHERS } from "../assets/images-new";
import { faqinfo, faqinfolong } from '../assets/data';
import styled from 'styled-components';

const faqhome = faqinfo.filter( item => item.home );

const Header= styled.div`
  text-align: left;
  padding: 6em 1em 3em 1em;
  h1 {
    margin: auto;
    max-width: 759px;
    font-size: 20px;
    line-height: 24px;
    color: #00326E;
  };
  @media (max-width: 875spx) {
    text-align: center;
  }
`;

const BadgeFaq = styled.div`
  @media (min-width: 720px){
    display: none;
  };
`

const content =
  <Header>
    <h1>{"Inicio > Preguntas Frecuentes"}</h1>
    <BadgeFaq>
      <BadgeList data={faqhome}/>
    </BadgeFaq>
    <CardList data={faqinfolong} />
  </Header>;

export default function Loading() {
  let history = useHistory();
  return (
    <div>
      <DecoImg src={OTHERS.FaqDeco}/>
      <KeoLogoImg src={OTHERS.LogoKeoPayback2} onClick={() => history.push("/payback")}/>
      <Content component={content} />
      <Footer />
    </div>
  )
};


const KeoLogoImg = styled.img`
  width: 179px;
  margin-left: 37px;
  margin-top: 30px;
  position: absolute;
`;

const DecoImg = styled.img`
  float: right;
  width: 193px;
  margin-top: -100px;
  margin-right: 50px;
  position: absolute;
  right: 0;
  @media (max-width: 720px) {
    margin-top: -100px;
    margin-right: 0px;
  };
`;
