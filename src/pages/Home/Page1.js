import React from "react";
import { PAGE1 } from "../../assets/images-new";
import StoreButtons from "../../components/StoreButtons";
import { Content } from "../../containers";
import styled from "styled-components";
import { Reveal, Tween } from 'react-gsap';
import { IOSLINK, ANDROIDLINK } from '../../constants'

export default function Page1(props) {
  const content =
  <PageWrapper ref={props.knowRef} >
    <Image>
    <Reveal trigger={<div />}>
      <Tween
        from={{ opacity: 0, transform: 'translate3d(0, 500px, 0)' }}
        duration={2}
        ease="circ.out"
      >
        <CircleBckgImg src={PAGE1.CardBckg} />
        <CardImg src={PAGE1.Card} />
      </Tween>
    </Reveal>
    </Image>
    <TextDiv>
    <Reveal trigger={<div />}>
      <Tween
        from={{ opacity: 0, transform: 'translate3d(0, 500px, 0)' }}
        duration={2}
        stagger={{ amount: 0.3, grid: [3, 1], axis: "y" }}
        ease="circ.out"
      >
      <TitleImg src={PAGE1.Title}/>
      <div className="ben-text">
        <p><span>¡KEO llega a México!</span> una empresa dispuesta a resolver
          tus problemas financieros. <Br /> <span>Otorgando</span> &nbsp;
          préstamos digitales, accesibles y <Br /> <span>sin revisar</span> &nbsp;
          historial crediticio para que disfrutes en <span>Tu Tarjeta KEO PAYBACK American Express®.</span> &nbsp;
        </p>
      </div>
      <p><span>Descárgala y solicítala*</span></p>
      <div>
        <div className="google">
        </div>
        <StoreButtons
          className="store-btn"
          onPressGoogle={() => window.open(ANDROIDLINK, "_blank")}
          onPressIos={() => window.open(IOSLINK, "_blank")}
        />
      </div>
      </Tween>
    </Reveal>
    </TextDiv>
  </PageWrapper>

  return (
    <Content component={content} />
  );
};

const Br = styled.br`
  display: none;
  @media (max-width: 720px){
    display: block;
  };
`;

const TitleImg = styled.img`
  width: 210px;
  margin: auto;
  margin-left: 12%;
  @media (max-width: 900px) {
    margin-left: 5%;
  }
  @media (max-width: 720px) {
    display: none;
  };
`;

const TextDiv = styled.div`
  @media (max-width: 720px) {
    text-align: center;
    padding: 0 5% 2em 5%;
  };
`;

const Image = styled.div`
  grid-col: 1;
  grid-row: 1;
  position: relative;
  overflow: hidden;
  @media (max-width: 720px) {
  min-height: 56vw;
    grid-col: 1;
    grid-row: 1;
  };
`;

const CircleBckgImg = styled.img`
  width: 469px;
  position: absolute;
  margin-top: -32px;
  @media (max-width: 975px) and (min-width: 720px) {
    width: 373px;
    margin-top: -15px;
  };
  @media (max-width: 720px) {
    display: none;
  };
`;

const CardImg = styled.img`
  width: 632px;
  position: absolute;
  margin-left: -30px;
  margin-top: -118px;
  @media (max-width: 975px) and (min-width: 720px) {
      width: 504px;
      margin-left: -30px;
      margin-top: -78px;
  };
  @media (max-width: 720px) {
      width: 130vw;
      margin-left: -15vw;
      margin-top: -32vw;
  };
`;

const PageWrapper = styled.div`
  display: grid;
  min-height: 480px;
  grid-template-columns: 50% 50%;
  max-width: 1080px;
  margin: auto;
  color: #2C2E3F;
  padding-top: 2.5em;

  .ben-text p {
    width: 70%;
    font-size: 0.9em;
  };

  .ben-text span {
    white-space: nowrap;
  };

  p span {
    font-weight: bold;
  };

  @media (max-width: 720px) {
    grid-template-columns: 100%;
    grid-template-row: 50% 50%;
    padding-top: 0.5em;

    .ben-text p {
      width: 90%;
      margin: auto;
      font-size: 0.8em;
    };
  };
`;
