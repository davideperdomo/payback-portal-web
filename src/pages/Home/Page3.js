import React from "react";
import { PAGE3 } from "../../assets/images-new";
import { benefits } from "../../assets/data"
import { Content } from "../../containers";
import styled from "styled-components";
import { Reveal, Tween } from 'react-gsap';

export default function Page3() {
  const content =
  <PageWrapper>
    <h2>¿Qué <span>beneficios</span> tienes?</h2>
    <CellContainer>
      <Cellphone>
        <img id="oval" src={PAGE3.OvalBckg} alt="oval" />
        <Reveal trigger={<div />}>
          <Tween
            from={{ opacity: 0, transform: 'translate3d(0, 500px, 0)' }}
            duration={2}
            stagger={{ amount: 0.3, grid: [7, 1], axis: "y" }}
            ease="circ.out"
          >
          <img id="phone" src={PAGE3.Phone} alt="phone" />
          <img id="card" src={PAGE3.Card}  alt="card" />
          <img id="arrow1" className="arrow" src={PAGE3.Arrow1} alt="arrow" />
          <img id="arrow2" className="arrow" src={PAGE3.Arrow2} alt="arrow" />
          <img id="arrow3" className="arrow" src={PAGE3.Arrow3} alt="arrow"/>
          <img id="arrow4" className="arrow" src={PAGE3.Arrow4} alt="arrow" />
          <img id="dots" className="arrow" src={PAGE3.Dots} alt="dots" />
          <img id="arrow4-res" src={PAGE3.Arrow4} alt="arrow" />
          </Tween>
        </Reveal>
      </Cellphone>
      <Reveal trigger={<div />}>
        <Tween
          from={{ opacity: 0, transform: 'translate3d(0, 500px, 0)' }}
          duration={2}
          stagger={{ amount: 0.4, grid: [4, 1], axis: "y" }}
          ease="circ.out"
        >
          <div style={{position: 'absolute'}}>
            <TextContainer top={150} left={15}>
              <p><span>{benefits[0].title}</span> <br/>
                {benefits[0].content}</p>
            </TextContainer>
            <TextContainer top={15} left={450}>
              <p><span>{benefits[1].title}</span> <br/>
                {benefits[1].content}</p>
            </TextContainer>
            <TextContainer top={550} left={20}>
              <p><span>{benefits[2].title}</span> <br/>
                {benefits[2].content}</p>
            </TextContainer>
            <TextContainer top={540} left={450}>
              <p><span>{benefits[3].title}</span> <br/>
                {benefits[3].content}</p>
            </TextContainer>
          </div>
        </Tween>
      </Reveal>
    </CellContainer>

  </PageWrapper>

  return (
    <Content component={content} background={'#00326E'} />
  );
};

const TextContainer = styled.div`
  color: #FFFFFF;
  position: absolute;
  width: 220px;
  margin-left: ${ (props) => props.left+'px'};
  margin-top: ${ (props) => props.top+'px' };
  p {
    font-size: 0.9em;
  }
  span {
   white-space: nowrap; 
  }
  @media (max-width: 720px) {
    position: relative;
    text-align: left;
    width: 80%;
    margin: auto;
    margin-top: 0px;
    margin-left: 0px;
  };
`;

const CellContainer = styled.div`
  width: 693px;
  margin: auto;
`;

const PageWrapper = styled.div`
  text-align: center;
  background-color: #00326E;
  min-height: 720px;
  padding: 3em;

  &&& h2 {
    color: #FFFFFF;
  };

  img {
    position: absolute;
  };

  @media (max-width: 720px) {
    min-height: 766px;
    padding: 3em 10vw;
  };
  @media (max-width: 400px) {
    padding: 2em 6vw 4em 6vw;
  };

`;

const Cellphone = styled.div`
  text-align: left;
  overflow: hidden;
  #oval {
    width: 693px;
  };
  #phone {
    width: 247px;
    margin-left: 209px;
    margin-top: 132px;
  };
  #card {
    width: 311px;
    margin-left: 332px;
    margin-top: 226px;
  };
  #arrow1 {
    width 100px;
    margin-left: 141px;
    margin-top: 286px;
  };
  #arrow2 {
    width: 41px;
    margin-left: 482px;
    margin-top: 144px;
  };
  #arrow3 {
    width: 77px;
    margin-left: 133px;
    margin-top: 448px;
  };
  #arrow4 {
    width: 100px;
    margin-left: 446px;
    margin-top: 456px;
  };
  #dots {
    width: 42px;
    margin-left: 633px;
    margin-top: 513px;
  };
  #arrow4-res {
    display: none;
  };
  #text1 {
    position: absolute;
    margin-top:180px;
  };
  @media (max-width: 720px) {
    min-height: 380px;
    margin: 0px 9vw;

    #oval, .arrow {
      display: none;
    };
    #phone {
      margin-left: 0px;
      margin-top: 0px;
      max-width: 250px;
    };
    #card {
      margin-left: 36px;
      margin-top: 82px;
      max-width: 250px;
    };
    #arrow4-res {
      display: block;
      width: 87px;
      margin-left: 184px;
      margin-top: 313px;
    };
  };
`;
