import React, { useState } from "react";
import { Content, Form } from "../../containers";
import { sendContact } from "../../services"
import styled from "styled-components";

export default function Page5() {
  const [ sended, setSended ] = useState('form');

  const sendMessage = async (form) => {
    setSended('loading');
    //sendform console.log('sendingform',form);
    const response = await sendContact(form);
    setSended('sended');
    return response;
  };

  const content =
    <PageWrapper>
      <h2>¿Soy elegible?</h2>
      <p>
        Recuerda que el préstamo está disponible solo para usuarios con invitación. Si <br />
        quieres ser parte del programa danos tu correo y te informaremos cuando <br />
        puedas solicitar tu préstamo.
      </p>
      <h2 id="resp-msg">Vamos a dar el primer paso</h2>
      <Form
        sended={sended}
        onDone={sendMessage} />
    </PageWrapper>;
  return (
    <Content component={content} />
  );
};

const PageWrapper = styled.div`
  text-align: center;
  padding: 6em 3em 3em 3em;
  min-height: 60vh;
  background-color: #F6F6F6;
  #resp-msg {
    display:none;
  };
  @media (max-width: 720px) {
    grid-template-columns: 100%;
    padding: 3em 4% 3em 6%;
    #resp-msg {
      display: block;
      color: #000000;
    };
  };
`;
