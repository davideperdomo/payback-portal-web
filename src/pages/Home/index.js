import React, { useRef, useState } from 'react';
import { useHistory } from "react-router-dom";
import { Footer, Modal } from '../../containers';
import HomePage from './HomePage';
import Page1 from "./Page1.js";
import Page2 from "./Page2.js";
import Page3 from "./Page3";
import Page4 from "./Page4";
import Page5 from "./Page5";
import Page6 from "./Page6";
import styled from 'styled-components';
import { HOME, PAGE2, PAGE3 } from "../../assets/images-new";
import { VIDEOLINK } from '../../constants';

export default function Home() {
  const [ videoModal, setVideoModal ] = useState(false);
  let history = useHistory();
  const knowRef = useRef(null);

  return (
    <MainContainer>
      <HomeContainer>
        <HomePage history={history} />
        <div className="knowdiv" onClick={() => knowRef.current.scrollIntoView({ behavior: 'smooth' })}>
          <p className="know">Conoce más</p>
          <img id="knowimg" src={HOME.ArrowDown} alt="know-more" />
        </div>
      </HomeContainer>
      <Page1 knowRef={knowRef} />
      <Page2 />
      <DecoImg src={PAGE2.Deco1}/>
      <Page3 />
        <DecoOval src={PAGE3.Deco1}/>
        <VideoLink onClick={()=>setVideoModal(true)}>
          <img src={HOME.IconPlay} alt="play" /> &nbsp;
          <p style={{fontWeight:'600',fontSize:'1em',color: '#00326E',textDecoration: 'underline'}}>
            Mira el video aquí
          </p>
        </VideoLink>
        <Modal open={videoModal} onClose={()=>setVideoModal(false)}>
          <div>
            <iframe
              title="video-mobile"
              width="560" height="315" style={{width: '100%'}}
              src={VIDEOLINK+'?autoplay=1'} frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen></iframe>
          </div>
        </Modal>
      <Page4 />
      <Page5 />
      <Page6 />
      <Footer />
    </MainContainer>
  )
};

const DecoOval = styled.img`
    height: 106px;
    position: absolute;
    right: 0;
    margin-top: -165px;
    margin-right: 3vw;
    @media (max-width: 950px) {
      margin-right: -5vw;
      height: 86px;
    };
`;

const VideoLink = styled.div`
  display: none;
  @media (max-width: 720px) {
    cursor: pointer;
    width: 60%;
    font-family: poppins;
    background: #FFFFFF;
    box-shadow: 0px 15px 38px 12px rgba(0,0,0,0.24);
    border-radius: 5.12px;
    margin-top: -1.1em;
    margin-bottom: 1.1em;
    margin-left: 21%;
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    font-weight: 600;
    font-size: 0.9em;
    letter-spacing: -0.5px;
    text-align: center;
    p {
      margin-right: 6vw;
    };
    a {
      margin-right: 18px;
      color: #FF4759;
      text-decoration: underline;
    };
  };

  @media (max-width: 340px) {
    width: 90%;
    margin-left: 4%;
  };
`;

const MainContainer = styled.div`
  position: relative;
  overflow: hidden;
  h2 {
    color: #00326E;
    letter-spacing: 0;
    text-align: center;
    line-height: 28px;
  };

  h3 {
    color: #00326E;
    letter-spacing: 0;
  };

  h2 span {
    color: #FF4759;
  };

  h3 span {
    color: #FF4759;
  };

  p {
    font-family: Poppins;
    font-size: 0.8em;
  };
  p span {
    font-weight: bold;
  };

  a, a:visited {
    color: #00326E;
  };

`;

const DecoImg = styled.img`
  height: 192px;
  position: absolute;
  margin-top: -96px;

  @media (max-width: 720px) {
    display: none;
  };
`;

const HomeContainer = styled.div`
  .deco1 {
    position: absolute;
    float: left;
    z-index:-1;
    margin: -120px 0 0 -30px;
    @media (max-width: 720px) {
      margin: -128px 0 0 0;
    };
  };
  .know {
    font-size: 0.8em;
    color: #2c2d3f;
    letter-spacing: -0.18px;
    text-align: center;
  };
  #knowimg {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 22px;
    margin-top: 0px;
    margin-bottom: 15px;
  };
  .deco2{
    position: absolute;
    margin: -170px 10px 10px 6%;
    z-index: -1;
    @media (max-width: 720px) {
      width: 70px;
      margin: -270px 10px 10px -40px;
    };
  }
  .c2btn {
    display: grid;
    margin: auto;
    max-width: 220px;
    margin-top: 40px;
    margin-bottom: 100px;
    @media (max-width: 720px) {
      margin-bottom: 40px;
    };
  }
  .subt-facts {
    font-weight: bold;
    font-size: 40px;
    color: #FF4759;
    letter-spacing: -0.5px;
    line-height: 49px;
  };
  .subt-faq {
    font-weight: bold;
    font-size: 26px;
    color: #FF4759;
    letter-spacing: -0.32px;
    line-height: 49px;
    margin-bottom: 5px;
  };
`;
