import React from "react";
import StoreButtons from "../../components/StoreButtons";
import { PAGE4 } from "../../assets/images-new";
import { Content, Carousel } from "../../containers";
import styled from "styled-components";
import { IOSLINK, ANDROIDLINK } from '../../constants'

export default function Page4() {
  const content =
    <PageWrapper>
      <h2>¿Cómo la <span>solicito</span>?</h2>
      <div>
        <Carousel
          Slides={[
            <div idx={0}>
              <OvalImg src={PAGE4.Carousel1Bckg} />
              <Phone src={PAGE4.Carousel1Phone} />
              <SlideText>
                Descarga la App y accede con tu
                código de acceso KEO que
                recibiste por correo electrónico.
              </SlideText>
            </div>
            ,
            <div idx={1}>
              <OvalImg src={PAGE4.Carousel2Bckg} />
              <Phone src={PAGE4.Carousel2Phone} />
              <SlideText>
                Crea tu cuenta, solicita tu préstamo y paga Tu Tarjeta.
              </SlideText>
            </div>,
            <div idx={2}>
              <OvalImg src={PAGE4.Carousel3Bckg} styles={{ width: '533px'}} />
              <Phone src={PAGE4.Carousel3Phone} />
              <SlideText>
                Llama a la línea de atención y sigue las instrucciones.
              </SlideText>
            </div>,
            <div idx={3}>
              <OvalImg src={PAGE4.Carousel4Bckg} />
              <Phone4 src={PAGE4.Carousel4Phone} />
              <Card src={PAGE4.Carousel4Card} />
              <SlideText>
                Llama a la línea de atención y sigue las instrucciones.
              </SlideText>
            </div>]}
        />
      </div>
      <h4>
        <span>Fácil, rápido y seguro.</span>
        ¡Ah! y lo mejor, sin <br/>
        tener que salir de casa.
      </h4>
      <StoreMessage>
        <p>Descárgala y solicítala*</p>
        <StoreButtons
          className="store-btn"
          onPressGoogle={() => window.open(ANDROIDLINK, "_blank")}
          onPressIos={() => window.open(IOSLINK, "_blank")}
        />
      </StoreMessage>
    </PageWrapper>

  return (
    <Content component={content} />
  );
};

const SlideText = styled.div`
  line-height: 1.4em;
  position: absolute;
  width: 173px;
  color: #FFFFFF;
  font-size: 0.7em;
  font-weight: 600;
  margin-left: 146px;
  margin-top: 315px;
  @media (max-width: 720px) {
    width: 40%;
    font-size: 0.7em;
    margin-left: 30%;
    margin-top: 310px;
  };
  @media (max-width: 500px) {
    width: 40%;
    font-size: 0.55em;
    margin-left: 30%;
    margin-top: 65vw;
  };
`;

const PageWrapper = styled.div`
  text-align: center;
  background-color: #F6F6F6;
  height: 704px;
  padding: 3em;

  p {
    font-weight: bold;
  };
  h4 span {
    color: #1dc0c0;
  };
  @media  (max-width: 720px) {
    padding: 4.5em 1em 1em 1em;
  };
`;

const OvalImg = styled.img`
  width: 466px;
  margin-top: 14px;
  position: absolute;
  margin-left: -230px;
  @media (max-width: 720px) {
    width: 111%;
    margin-left: -57%;
  };
`;

const Phone = styled.img`
  width: 168px;
  position: absolute;
  margin-left: -71px;
  @media (max-width: 720px) {
    width: 38%;
    margin-left: -17%;
  };
`;

const Phone4 = styled.img`
  width: 220px;
  position: absolute;
  margin-left: -127px;
  @media (max-width: 720px) {
    width: 51%;
    margin-left: -33%;
  };
`;

const Card = styled.img`
  width: 357px;
  position: absolute;
  margin-left: -139px;
  margin-top: 27px;
  @media (max-width: 720px) {
    width: 78%;
    margin-left: -33%;
    margin-top: 22px;
  };
`;

const StoreMessage = styled.div`
  width: 240px;
  margin: auto;
  p {
    font-weight: bold;
    font-size: 0.9em;
  };
`;
