import React, { useEffect, useState } from 'react';
import { Content, CarouselHome, Modal } from '../../containers';
import { StoreButtons } from '../../components';
import { HOME } from "../../assets/images-new";
import policiesKeoPdf from "../../assets/politicasKeo.pdf";
import styled from 'styled-components';
import {
  POLICIESPAYBACKLINK,
  IOSLINK,
  VIDEOLINK,
  ANDROIDLINK } from '../../constants'

export default function HomePage() {
  const [isCharged, SetIsCharged ] = useState(false);
  const [videoModal, setVideoModal] = useState(false);
  useEffect( () => {
    window.scrollTo(0, 0);
    window.addEventListener('DOMContentLoaded', (event) => {
      SetIsCharged(true);
    });
  }, [] );

  const content =
    <div className="how" style={{position: "relative", overflow: "hidden"}}>
      <LogoImg src={HOME.LogoKeoPayback} />
      <Header>
        <Message>
            <h1>
              Este año deja tus
              preocupaciones <br />
              a un lado
            </h1>
            <br/><br/><br/>
            <StoreDiv>
              <p className="par"><span>Descárgala y solicítala*</span></p>
              <StoreButtons
                className="store-btn"
                onPressGoogle={() => window.open(ANDROIDLINK, "_blank")}
                onPressIos={() => window.open(IOSLINK, "_blank")}
              />
            </StoreDiv>
            <DecoImg src={HOME.DecoDots}/>
        </Message>
        <CarouselContainer>
          <CarouselHome
          Slides={[
            <ContainerImg>
              <p className="par">Llega el primer préstamo en una Tarjeta<br/> que te da <span>Puntos PAYBACK</span> por todas tus compras.</p>
              <HeaderImg src={HOME.CarouselBckg1}/>
              <HeaderImg isPerson src={HOME.CarouselImg1}/>
            </ContainerImg>,
            <ContainerImg>
              <p className="par">No dejes de comprar donde te gusta,<br/> acumula <span>Puntos PAYBACK</span> en todas<br/> tus tiendas favoritas..</p>
              <HeaderImg src={HOME.CarouselBckg2}/>
              <HeaderImg isPerson src={HOME.CarouselImg2}/>
            </ContainerImg>,
            <ContainerImg>
              <p className="par"><span>Solicita un préstamo KEO</span>, lo tienes al <br/> alcance de tu mano.</p>
              <HeaderImg src={HOME.CarouselBckg2}/>
              <HeaderImg isPerson src={HOME.CarouselImg3}/>
            </ContainerImg>
          ]}
          />
        </CarouselContainer>
      </Header>
      <VideoLink onClick={()=>setVideoModal(true)}  >
        <p>La Tarjeta que hará de este, tu año </p> &nbsp;
        <img className="play-btn" src={HOME.IconPlay} alt="play" /> &nbsp;
        <p style={{fontWeight:'600',fontSize:'1em',color: '#00326E',textDecoration: 'underline',cursor: 'pointer'}}>
          Mira el video aquí
        </p>
      </VideoLink>
      <Modal open={videoModal} onClose={()=>setVideoModal(false)}>
        <div>
          <iframe
            title="video-desktop"
            width="560" height="490" style={{width: '100%', maxHeight: '80vh'}}
            src={VIDEOLINK+'?autoplay=1'} frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen></iframe>
        </div>
      </Modal>
      <Policies>
        <a href={policiesKeoPdf}  target="_blank" rel="noreferrer">
          Políticas de privacidad KEO
        </a>  &nbsp; - &nbsp;
        <a href={POLICIESPAYBACKLINK} target="_blank" rel="noreferrer">
          Políticas de privacidad PAYBACK
        </a>
      </Policies>
    </div>;

  return (
    <Page isCharged={isCharged}>
      <Content component={content} />
    </Page>
  );
};

const DecoImg = styled.img`
  width:220px;
  margin: 2em 1em;
  @media (max-width: 720px) {
    display: none;
  };
`;

const StoreDiv = styled.div`
  @media (max-width: 720px) {
    display: none;
  };
`;

const Policies = styled.div`
   display: flex;
   align-items: center;
   justify-content: center;
    margin-top: -50px;
    margin-bottom: 50px;

   a {
     font-weight: 600;
     font-size: 0.7em;
     text-decoration: underline;
   };

   @media (max-width: 720px) {
      padding: 1em;
      text-align: center;
      font-size: 0.9em;
      margin-bottom: 0px;
   }
 `;

const ContainerImg = styled.div`
  position: relative;
`;

const VideoLink = styled.div`
  width: 580px;
  margin: auto;
  font-family: poppins;
  background: #FFFFFF;
  box-shadow: 0px 15px 38px 12px rgba(0,0,0,0.24);
  border-radius: 5.12px;
  margin-top: 1em;
  margin-bottom: 69px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 600;
  font-size: 0.9em;
  letter-spacing: -0.5px;
  text-align: center;
  a {
    color: #FF4759;
    text-decoration: underline;
  };
  .play-btn {
    cursor: pointer;
  };
  @media (max-width: 720px) {
    display: none;
  };
`;

const Page = styled.div`
  overflow: ${props => props.isCharged?'inherit':'hidden'};
`;

const LogoImg = styled.img`
  width: 500px;
  position: absolute;
  right: 0px;
  z-index: 5;
  @media (max-width: 720px) and (min-width: 520px) {
    width: 80%;
    right: 0;
  };
  @media (max-width: 520px) {
    width: 100%;
    right: 0;
  };
`;

const Header = styled.div`
  display: block;
  h1 {
    font-family: Poppins-Bold;
    font-size: 2.5em;
    color: #00326E;
    line-height: 1.1em;
    letter-spacing: -0.5px;
  };
  h1 span {
    color: #00BBBB;
  };
  @media (max-width: 720px) {
    display: grid;
    grid-template-rows: 65% 35%;
    padding: 0em 0% 3em 0%;
  };
  @media (max-width: 520px) {
    grid-template-rows: 73% 27%;
  };
  @media (max-width: 330px) {
    grid-template-rows: 66% 34%;
  };
`;

const Message = styled.div`
position: absolute;
margin-left: 16%;
max-width: 340px;
  margin-top: 8em;
  z-index: 2;
  min-width: 357px;
  grid-column:1;
  .store-btn {
    margin-left: auto;
    margin-right: auto;
  };
  .par {
    font-size: 0.95em;
  };
  @media (max-width: 720px) {
    position: relative;
    margin-top: 25px !important;
    padding-top: 0px !important;
    min-width: 0px;
    width: 43vw;
    margin-left: 28vw;
    text-align: center;
    grid-column: 1;
    grid-row: 2;
    h1 {
      font-size: 1.5em;
    };
  };
  @media  (max-width: 520px){
    width: 65vw;
    margin-left: 16vw;
  };
`;

const CarouselContainer = styled.div`
  grid-column: 2;
  grid-row: 1;
  @media (max-width: 720px) {
    grid-row: 1;
    grid-column: 1;
    min-height: 554px;
  };
  @media (max-width: 520px) {
    grid-row: 1;
    grid-column: 1;
    min-height: 500px;
  };
  @media (max-width: 333px) {
    min-height: 400px;
  };
`;

const HeaderImg = styled.img`
  margin-left: auto;
  margin-right: 1em;
  order: 2;
  width: 50%;
  min-width: 556px;
  margin-bottom: ${props=> props.isPerson ? '0' : '-26px'};
  margin-top: ${props=> props.isPerson ? '-48.5vw' : '-12vw'};
  z-index: ${props=> props.isPerson ? '5' : '1'};
  grid-column: 2;
  z-index: 1;
  @media (max-width: 1100px) {
    margin-top: ${props=> props.isPerson ? '-534px' : '-12vw'};
  }
  @media (max-width: 720px) and (min-width: 520px) {
    width: 95vw;
    grid-row: 1;
    grid-column: 1;
    margin-left: -6vw;
    margin-top: ${props=> props.isPerson ? '-115vw' : '-20vw'};
  };
  @media (max-width: 520px) {
    min-width: 520px;
    margin-top: -110vw;
    grid-row: 1;
    grid-column: 1;
    margin-left: -22vw;
    margin-top: ${props=> props.isPerson ? '-132vw' : '-20vw'};
  };
  @media (max-width: 320px) {
    min-width: 420px;
  };
`;
