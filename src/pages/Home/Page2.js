import React from "react";
import { StoreButtons, TileList } from "../../components";
import { Content } from "../../containers";
import { steps } from '../../assets/data';
import styled from "styled-components";
import { Reveal, Tween } from 'react-gsap';
import { IOSLINK, ANDROIDLINK } from '../../constants'

export default function Page2(props) {
  const content =
  <PageWrapper>
    <h2>Si este <span>2021...</span></h2>
    <TileContainer>
      <TileList data={steps} />
    </TileContainer>
    <Reveal repeat trigger={<div />}>
      <Tween
        from={{ opacity: 0, transform: 'translate3d(0, 500px, 0)' }}
        duration={2}
        stagger={{ amount: 0.3, grid: [1, 3], axis: "x" }}
        ease="circ.out"
      >
        <div className="benefits-text">
          <h3>
            Descarga la App KEO, y solicita Tu <br />
            <span>Tarjeta KEO PAYBACK </span> American Express®. <br />
            No lo dejes a la suerte y hazlo realidad.
          </h3>
        </div>
        <h4>¿Qué esperas?</h4>
        <div className="store-btn">
          <StoreButtons
            onPressGoogle={() => window.open(ANDROIDLINK, "_blank")}
            onPressIos={() => window.open(IOSLINK, "_blank")}
          />
        </div>
      </Tween>
    </Reveal>
  </PageWrapper>

  return (
    <Content component={content} background={"#F6F6F6"}/>
  );
};

const TileContainer = styled.div`
  width: 814px;
  margin: auto;
  @media (max-width: 720px) {
    width: 100%;
  };
`;

const PageWrapper = styled.div`
  text-align: center;
  background-color: #F6F6F6;
  min-height: 627px;
  padding: 3em;
  overflow: hidden;
  position: relative;

  .store-btn {
    width: 229px;
    margin: auto;
  };

  .benefits-text {
    span {
      white-space: nowrap;
    };
  };

  h4 {
    color: #00326E;
    font-weight: bold;
  };

  @media (max-width: 720px) {
    padding: 1em 1em 3em 1em;
    h3 {
      font-size: 1em;
    };
  };
`;
