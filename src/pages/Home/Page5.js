import React from "react";
import { PAGE5 } from "../../assets/images-new";
import { BadgeList } from '../../components';
import { faqinfo } from '../../assets/data';
import { Content } from "../../containers";
import styled from "styled-components";
import { Reveal, Tween } from 'react-gsap';

const faqhome = faqinfo.filter( item => item.home );

export default function Page5() {
  const content =
    <div>
      <PageWrapper>
        <Reveal trigger={<div />}>
          <Tween
            from={{ opacity: 0, transform: 'translate3d(0, 500px, 0)' }}
            duration={2}
            ease="circ.out"
          >
            <PersonImg src={PAGE5.Person} />
          </Tween>
        </Reveal>
        <Reveal trigger={<div />}>
          <Tween
            from={{ opacity: 0, transform: 'translate3d(0, 500px, 0)' }}
            duration={2}
            stagger={{ amount: 0.3, grid: [1, 3], axis: "x" }}
            ease="circ.out"
          >
          <h2>¿Cuáles son las <span>condiciones</span>?</h2>
          <Conditions>
            <p>
              <span>Costo de apertura:</span> <br /><br />
              Costo único por <span>200 MXN que solo tienes que pagar una vez. Cubre la impresión y entrega de Tu Tarjeta</span>
            </p>
            <div id="rates">
              <p>
                <span>Tasa de interés:</span> <br />
                9.59% MV
              </p>
              <p>
                <span>Tasa de mora:</span> <br/>
                20% MV
              </p>
            </div>
              <p>
                <span>Comision por disposiciones de efectivo:</span> <br/>
                MXN 46
              </p>
          </Conditions>
          <ConditionsMessage>
            <p>
              <span>Uso:</span> <br />
              El préstamo tiene un programa de pagos para
              3 meses. Si pagas a tiempo el primer mes, no
              generas intereses.
            </p>
          </ConditionsMessage>
          <ConditionsMessage>
            <p>
              <span>Pago:</span> <br />
              Puedes pagar tu préstamo fácilmente a través
              de transferencia bancaria o en efectivo.
            </p>
          </ConditionsMessage>
          </Tween>
        </Reveal>
      </PageWrapper>
      <FaqSection>
        <h2>¿Te quedaron <span>dudas</span>?</h2>
        <p>
          Revisa las Preguntas Frecuentes, acá puedes encontrar la <br />
          respuesta que buscas.
        </p>
        <BadgeList data={faqhome}/>
      </FaqSection>

      <FaqLink>
        <p className="faqlink">¿Tienes dudas sobre el préstamo? consulta nuestras &nbsp;
        <a href="/payback/faq"> preguntas frecuentes</a> </p>
      </FaqLink>
    </div>

  return (
    <Content component={content} />
  );
};

const PersonImg = styled.img`
  width: 368px;
  @media (max-width: 720px) {
    display: none;
  };
`;

const Conditions = styled.div`
  width: 330px;
  margin: auto;
  p {
    font-size: 0.9em;
    width: 380px;
  };

  #rates {
    display: flex;
  }

  @media (max-width: 720px) {
    width: 100%;
    p {
      width: 99%;
    };
  };
`;

const ConditionsMessage = styled.div`
  width: 340px;
  padding: 2px 15px;
  margin: 20px auto;
  box-shadow: 0 20px 35px 15px rgba(2,10,32,0.20);
  background-color: #FFFFFF;
  border-radius: 10px;
  font-size: 0.9em;
  @media (max-width: 720px) {
    width: 90%;
  };
`;

const PageWrapper = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
  padding: 6em 15% 3em 15%;
  @media (max-width: 1024px) and (min-width: 720px) {
    padding: 4em 2% 3em 2%;
  };

  @media (max-width: 720px) {
    grid-template-columns: 100%;
    padding: 3em 6% 3em 6%;
  };
`;

const FaqSection = styled.div`
  text-align: center;
  font-family: Poppins;
  padding-bottom: 4em;
  margin: auto;
  p {
    font-size: 0.9em;
  };

  @media (max-width: 720px) {
    display: none;
  };
`;

const FaqLink = styled.div`
  width: 580px;
  font-family: poppins;
  background: #FFFFFF;
  box-shadow: 0px 15px 38px 12px rgba(0,0,0,0.24);
  border-radius: 5.12px;
  position: absolute;
  left: 50%;
  margin-left:-290px;
  margin-top: -1.5em;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 0.9em;
  letter-spacing: -0.1px;
  text-align: center;
  a {
    font-weight: 600;
    text-: decoration: underline;
    color: #303643;
  };
  @media (max-width: 720px) {
    width: 100%;
    box-shadow: none;
    position: relative;

    left: 0;
    margin-left: 0;
    margin-top: 0;

    .faqlink {
      padding: 0em 4em;
    };
  };
`;
