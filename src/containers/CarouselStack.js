import React, {useState} from 'react';
import styled from 'styled-components';
import { PAGE4 } from '../assets/images-new'
import { PlayState, Tween } from 'react-gsap';

const Carousel = ({ Slides}) => {
  const length = Slides.length;
  const [ currentSlide, setCurrentSlide ] = useState(0);
  const [ currentDisplay, setCurrentDisplay ] = useState(0);
  const [ slidesPlay, setSlidesPlay ] = useState([
    PlayState.play,
    PlayState.stop,
    PlayState.pause,
    PlayState.pause]);

  const handleNext = () =>{
    console.log("next", slidesPlay, currentSlide, Slides[0].props.children);
    let newPlay=[...slidesPlay];
    if ( currentSlide+1 < length ) {
      setCurrentSlide( currentSlide+1 );
      if ( currentSlide === 0 )
        newPlay[currentSlide]=PlayState.pause;
      newPlay[currentSlide+1]=PlayState.play;
      setSlidesPlay(newPlay);
      setTimeout(() =>
      {setCurrentDisplay(currentDisplay+1);console.log("curr",currentSlide, currentDisplay);}, 1500);
    };
  };

  const handlePrev = () =>{
    //console.log("prev",slidesPlay, currentSlide);
    let newPlay=[...slidesPlay];
    newPlay[currentSlide]=PlayState.reverse;
    setSlidesPlay(newPlay);
    if (currentSlide-1 >= 0 ) setCurrentSlide( currentSlide-1 );
    if (currentSlide-1 >= 0 ) setCurrentDisplay( currentDisplay-1 );
  };


  return (
    length > 0 && (
      <CarouselWrapper >
        <Slide idx={0} curr={currentDisplay}>
          {Slides[0].props.children.map(item=>item)}
        </Slide>
        <Slide idx={1} curr={currentDisplay}>
          <Tween
            from={{ x: '663px', opacity: 0}} to={{ x: '0px', opacity: 1 }}
            playState={slidesPlay[1]} duration={1.3}
            stagger={{ amount: 0.3, grid: [1, 3], axis: "x" }}
            ease="sine.out"
            >{Slides[1].props.children.map(item=>item)}
          </Tween>
        </Slide>
        <Slide idx={2} curr={currentDisplay}>
          <Tween
            from={{ x: '663px', opacity: 0}} to={{ x: '0px', opacity: 1 }}
            playState={slidesPlay[2]} duration={1.3}
            stagger={{ amount: 0.3, grid: [1, 3], axis: "x" }}
            ease="sine.out"
            >{Slides[2].props.children.map(item=>item)}
          </Tween>
        </Slide>
        <Slide idx={3} curr={currentDisplay}>
          <Tween
            from={{ x: '663px', opacity: 0}} to={{ x: '0px', opacity: 1 }}
            playState={slidesPlay[3]} duration={1.3}
            stagger={{ amount: 0.3, grid: [1, 4], axis: "x" }}
            ease="sine.out"
            >{Slides[3].props.children.map(item=>item)}
          </Tween>
        </Slide>
        <ButtonControl onClick={()=> handlePrev() } left >
          <img src={PAGE4.CarouselArrowLeft}  alt="left" />
        </ButtonControl>
        <ControlLabel>{`${currentSlide+1}/${length}`}</ControlLabel>
        <ButtonControl onClick={()=> handleNext() }>
          <img src={PAGE4.CarouselArrowRight} alt="right" />
        </ButtonControl>
      </CarouselWrapper>
    )
  );
};

const CarouselWrapper = styled.div`
  position: relative;
  max-width: 466px;
  min-height: 416px;
  margin: auto;
  @media (max-width: 720px) {
    width: 88vw;
    max-width: 414px;
  };
`;

const ButtonControl = styled.button`
  border: none;
  width: 36px;
  height: 36px;
  border-radius: 18px;
  position: absolute;
  margin-top: 200px;
  background-color: #FFFFFF;
  box-shadow: 0 0 25px -4px rgba(0,0,0,0.4);
  margin-left: ${props => props.left ? "-52%" : "45%"};
  img {
    margin: 6px 3px 3px 5px;
  };
  &:hover {
    border: none;
    outline: none;
    background-color: #1dc0c0;
  };
  @media (max-width: 720px) {
    margin-left: ${props => props.left ? "-54%" : "45%"};
    img {
      position: absolute;
      margin: -6px;
    };
  };
`;

const ControlLabel = styled.p`
  font-style: italic;
  font-weight: bold;
  font-size: 1em;
  margin-left: 222px;
  margin-top: 407px;
  z-index: 5;
  position: absolute;
  @media (max-width: 720px) {
    margin-left: 48%;
  };
`;

const Slide = styled.div`
  display: ${props => props.idx >= props.curr ? 'block' : 'none'};
`;

export default Carousel;

/*

  {Slides.map( (item, idx) => {
    return <Tween
      to={{  transform: 'translate3d(-663px, 0, 0)' }}
      duration={2}
      PlayState={ (idx==0)? PlayState.stop : PlayState.play}
      ease="circ.out"
    ><Slide idx={idx}>{item}</Slide></Tween>

  })}
*/
