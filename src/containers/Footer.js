import React from 'react'
import { OTHERS } from "../assets/images-new";
import styled from 'styled-components';

const FACEBOOK_URL="https://www.facebook.com/KEOLATAM/"
const INSTAGRAM_URL="https://www.instagram.com/keolatam/"

export default function Footer(props) {
	return (
		<FooterWrapper isContact={props.isContact}>
			<FooterContainer>
				<img id="logokeo" src={OTHERS.LogoKeoPayback2} alt="logokeo" />
				<div id="footer-text">
					Esto es posible gracias a la <br />
					<span>alianza entre KEO y PAYBACK</span>
				</div>
				<a href={FACEBOOK_URL} target="_blank" rel="noreferrer">
					<img id="logo-social" src={OTHERS.FooterIconFacebook} alt="facebook - keo" />
				</a>
				<a href={INSTAGRAM_URL} target="_blank" rel="noreferrer">
					<img id="logo-social" src={OTHERS.FooterIconInstagram} alt="instagram - keo" />
				</a>
				<div className="terms">
					<div className="termscol">
						<p>*Recuerda que el préstamo está disponible solo para usuarios con invitación.<br />  </p>
						<p>Debes tener tu código de acceso KEO que recibiste por correo electrónico.</p>
					</div>
					<div className="termscol" id="left">
						<p><span>American Express®</span> es una marca de <span>American Express</span>. La Tarjeta KEO PAYBACK. <br /> </p>
						<p><span>American Express®</span> es emitida por KEO bajo licencia de <span>American Express</span>. </p>
					</div>
				</div>
				<div className="terms-cat">
					<b>CAT</b> sin IVA del <b>294%</b> calculado para un periodo comprendido del 30 de enero del 2021 al 30 de abril del 2021. Dicho cálculo está basado en un crédito de $4,000 pesos, con una <b>tasa de interés nominal anual del 115%</b>, para un plazo de 3 meses; y representa el costo anual total del financiamiento expresado en términos porcentuales anuales. El cálculo anterior incluye la comisión por apertura, así como todas las demás comisiones aplicables y se excluyen los impuestos, así como, los costos correspondientes a trámites, servicios prestados por terceros. Lo anterior para fines informativos y de comparación exclusivamente.
				</div>
			</FooterContainer>
		</FooterWrapper>
	)
};


const FooterWrapper = styled.div`
	position: ${(props) => props.isContact?"absolute":"inherit"};
	width: 100%;
	bottom: ${(props) => props.isContact?"0":"auto"};
	@media (max-width: 720px) {
		bottom: auto;
	};
`;

const FooterContainer = styled.div`
	max-width: 1030px;
	margin: auto;
	font-weight: normal;
	font-size: 0.92em;
	line-height: 20px;
	padding: 3em 0.1em 2em 0.1em;
	text-align: center;

	span {
			font-weight: bold;
	};

	#footer-text {
		margin: 0.5em 0;
	}
	#logokeo {
		width: 284px;
    	margin: 2em;
	};

	#logo-social {
		width: 159px;
		margin: 2em;
	};

	.terms {
		display: grid;
		grid-template-columns: 50% 50%;
		text-align: left;
		span {
			font-weight: normal;
			white-space: nowrap;
		}
	};
	.terms-cat {
		margin-top: 1em;
    	font-size: 0.8em;
		text-align: left;
		span {
			font-size: 1.3em;
			font-weight: normal;
		}
	};
	#left {
    border-left: 1px solid gray;
    padding-left: 1em;
	};
	p {
		line-height: 1em;
		font-size: 0.8em;
		color: #000000;
		letter-spacing: 0.09px;
	};

	a {
		text-decoration: none;
		font-weight: normal;
		font-size: 12px;
		letter-spacing: 0;
		line-height: 25px;
	};


	@media (max-width: 720px) {
		padding: 3em 2em 2em 2em;
		grid-template-columns: 45% 55%;
		grid-template-rows: 70px 110px;
		font-size: 12px;
		line-height: 20px;

		.terms {
			grid-template-columns: 100%;
			grid-template-rows: 50% 50%;
			text-align: left;
    	font-size: 0.9em;
		};

		#left {
	    border-left: none;
	    padding-left: 0;
		};
	};
`;
