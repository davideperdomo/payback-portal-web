import React from 'react';
import { WithStore } from 'pure-react-carousel';

function SlideLabel(props) {
  const state = props.carouselStore.getStoreState();
  setTimeout(()=>console.log(state) , 10000);
  return (
    <p style={props.style}>{`${state.currentSlide+1}/${state.totalSlides}`}</p>
  );
};

export default WithStore(SlideLabel,  (state) => {
    return {currentSlide: state.currentSlide}
  });
