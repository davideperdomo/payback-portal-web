import React, {useState} from 'react';
import styled from 'styled-components';
import { PAGE4 } from '../assets/images-new'

const OFFSET = 100;//1250;

const Carousel = ({ Slides }) => {
  const length = Slides.length;

  const [ currentSlide, setCurrentSlide ] = useState(0);
  const [ play, setPlay ] = useState(false);

  const handleNext = () =>{
    //console.log("next", currentSlide, Slides[0].props.children);
    if ( currentSlide+1 < length ) {
      setPlay(`${-OFFSET*(currentSlide+1)}`);//px`);
      setCurrentSlide( currentSlide+1 );
    }
    else {
      setPlay((`-${OFFSET*(0)}`));
      setCurrentSlide( 0 );
    }
  };

  const handlePrev = () =>{
    //console.log("prev", currentSlide, OFFSET*(currentSlide-1));
    if (currentSlide-1 >= 0 ) {
      setPlay(`-${OFFSET*(currentSlide-1)}`);//px`);
      setCurrentSlide( currentSlide-1 );
    }
    else {
      setPlay((`-${OFFSET*(2)}`));
      setCurrentSlide( 2 );
    };
  };

  return (
    length > 0 && (
      <CarouselWrapper>
        <Slide >
          <AnimText move={play}>{Slides[0].props.children[0]}</AnimText>
          <Anim move={play} isCurrent={(currentSlide === 0)}>{Slides[0].props.children[1]}</Anim>
          <Anim move={play} isCurrent={(currentSlide === 0)} child>{Slides[0].props.children[2]}</Anim>
        </Slide>
        <Slide >
          <AnimText move={play}>{Slides[1].props.children[0]}</AnimText>
          <Anim move={play} isCurrent={(currentSlide === 1)}>{Slides[1].props.children[1]}</Anim>
          <Anim move={play} isCurrent={(currentSlide === 1)} child>{Slides[1].props.children[2]}</Anim>
        </Slide>
        <Slide>
          <AnimText move={play}>{Slides[2].props.children[0]}</AnimText>
          <Anim move={play} isCurrent={(currentSlide === 2)}>{Slides[2].props.children[1]}</Anim>
          <Anim move={play} isCurrent={(currentSlide === 2)} child>{Slides[2].props.children[2]}</Anim>
        </Slide>

        <CarouselControl>
          <ButtonControl onClick={()=> handlePrev() } left >
            <img src={PAGE4.CarouselArrowLeft} alt="left" />
          </ButtonControl>
          <ControlLabel>{`${currentSlide+1}/${length}`}</ControlLabel>
          <ButtonControl onClick={()=> handleNext() } >
            <img src={PAGE4.CarouselArrowRight} alt="right"/>
          </ButtonControl>
        </CarouselControl>
      </CarouselWrapper>
    )
  );
};

const CarouselWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 50px;
  min-height: 563px;
  overflow-x: hidden;
  margin: auto;
  scroll-snap-type: x mandatory;
  scroll-behavior: smooth;
  -webkit-overflow-scrolling: touch;
   @media (max-width: 720px) and (min-width: 520px){
      width: 100%;
      margin: 0;
      min-height: 477px;
    };
  @media (max-width: 520px){
    min-height: 410px;
    margin: 0;
  };
`;

const Slide = styled.div`
  width: 100%;
`;

const AnimText = styled.div`
  width: 100%;
  z-index: 5;
  margin-left: 16%;
  position: absolute;
  margin-top: 314px;
  //margin-right: ${`${OFFSET-0}vw`};
  flex-shrink: 0;
  transition: all 1.3s cubic-bezier(0, 0, 0.77, 1.11);
  transform: ${ props => props.move? 'translate('+props.move+'%)':'translate(0px)' };
  @media (max-width: 720px){
    width: 70vw;
    margin-left: 25vw;
    transform: ${ props => props.move? 'translate('+props.move*1.1+'vw)':'translate(0px)' };
    margin-top: 650px;
    text-align: center;
  };
  @media (max-width: 520px){
    margin-left: 15vw;
    transform: ${ props => props.move? 'translate('+props.move*1.4+'vw)':'translate(0px)' };
  };
  @media (max-width: 330px){
    margin-top: 529px;
  };
`;

const Anim = styled.div`
width: 100vw;
margin-left: 40%;
  margin-right: ${`${OFFSET-0}vw`};
  flex-shrink: 0;

  opacity: ${props => props.isCurrent ? '1' : '0'};
  transition: opacity 1.3s ease-in-out;
  transition: ${ props => props.child ? 'all 0.5s cubic-bezier(0, 0, 0.45, 0.96)':'all 1s cubic-bezier(0, 0, 0.45, 0.96)'};
  transform: ${ props => props.move? 'translate('+props.move+'vw)':'translate(0px)' };

  @media (max-width: 720px) and (min-width: 520px) {
    margin: 0;
    margin-right: 6vw;
  };
  @media (max-width: 520px){
    margin: 0;
    margin-right: 40vw;
    transform: ${ props => props.move? 'translate('+(props.move*1.4)+'vw)':'translate(0px)' };
  };
`;


const CarouselControl = styled.div`
  position: absolute;
  margin-top: 487px;
  margin-left: 82%;
  width: 124px;
  display: grid;
  grid-template-columns: 30% 40% 30%;
  text-align: center;
  @media (max-width: 720px) {
    margin-left: -62px;
    left: 50%;
  };
  @media (max-width: 330px) {
    margin-top: 398px;
  };
`;

const ButtonControl = styled.button`
  border: none;
  width: 36px;
  height: 36px;
  border-radius: 18px;
  background-color: #FFFFFF;
  box-shadow: 0 0 25px -4px rgba(0,0,0,0.4);
  img {
    margin: 6px 3px 3px 5px;
  };
  &:hover {
    border: none;
    outline: none;
    background-color: #1dc0c0;
  };
  @media (max-width: 720px) {
    img {
      position: absolute;
      margin: -6px;
    };
  }
`;

const ControlLabel = styled.p`
  font-style: italic;
  font-weight: bold;
  font-size: 1em;
  z-index: 5;
`;


export default Carousel;

/*
  {Slides.map( item => {
    return (
      <Slide>{item}</Slide>
    )
  })}
*/
