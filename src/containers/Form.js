import React, { useState, useEffect } from "react";
import { Input } from "../components";
import styled from "styled-components";
import { PAGE6 } from "../assets/images-new";
import policiesKeoPdf from "../assets/politicasKeo.pdf";
import { Reveal, Tween } from 'react-gsap';
import ReactLoading from 'react-loading';

const POLICIESKEO = policiesKeoPdf;
const INITFORM = {
  'name': null,
  'email': null,
  'acept': false
};
const INITERRORS = {
  'name': [],
  'email': [],
  'acept': []
};

export const validateEmail = (email) => {
  return RegExp(/^[\w-w.]+@([\w-]+\.)+[\w-]{2,4}$/).test(email)
    ? true
    : false;
}

export default function Form(props) {
  const [ form, setForm ] = useState(INITFORM);
  const [ errors, setErrors ] = useState(INITERRORS);
  const [ validating, setValidating] = useState(false);

  const handleInputChange = (event) => {
    const name = event.target.name;
    console.log(event.target.value);
    const value = name === 'acept' ? !form.acept : event.target.value
    setForm({
      ...form,
      [name] : value
    });
  };

  const validateForm = () => {
    const newError = {...errors};
    //name
    if ( form.name === null || form.name === "" ) newError.name = ["El campo Nombre es obligatorio. "];
    else newError.name = [];
    //email
    if ( form.email === null || form.email === "" ) newError.email = ["El campo Email es obligatorio. "];
    else if ( !validateEmail(form.email) ) newError.email = ["El Email no tiene un formato válido. "]
    else newError.email = [];
    //name
    if ( form.acept === false ) newError.acept = ["Debes aceptar la politica de privacidad. "];
    else newError.acept = [];
    //update error
    setErrors(newError);
  };

  const getMsgError = () => {
    return (
      <React.Fragment>
      { validating ?
        <ErrorMsg>
          {
            Object.entries(errors).map( (err, idx) => {
              return ( (err[1] && err[1].length > 0) ? err[1] : '')
            })
          }
        </ErrorMsg>
        : <></>
      }
      </React.Fragment>
    )
  };

  const checkHasError = () => {
    var formHasError = false;
    Object.entries(errors).forEach( err => {
      if ( err[1].length > 0 ) {
        //console.log("hasError");
        formHasError = true;
      };
    });
    return formHasError;
  };

  useEffect( () => {
    validateForm();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form] );

  const handleSubmit = async () => {
    setValidating(true);
    validateForm();
    if ( !checkHasError() ) {
      //console.log("acepted");
      const response = await props.onDone(form);
      console.log("response", (response) ? 'ok' : 'error');
    };
  };

  return (
    <div>
      <TopBorder />
      <Container>
      { (props.sended === 'form' ) ?
        <FormContent>
          <Label> Nombre* </Label>
          <Input className='margin-bottom' name='name'
            onChange={handleInputChange}
            warning={validating && (errors.name.length>0)}
            placeholder='' required />
          <Label> Correo electrónico*</Label>
          <Input className='margin-bottom' type='email' name='email'
            onChange={handleInputChange}
            warning={validating && (errors.email.length>0)}
            placeholder='' required />

          <Checkbox  warning={validating && (errors.acept.length>0)} isChecked={form.acept}>
            <div id="radio-btn" />
            <input type="checkbox" id="input-base" name="acept" onChange={handleInputChange}/>
            <label htmlFor="other">
              &nbsp;&nbsp; He leido y acepto la &nbsp;
              <a className="policy-link" href={POLICIESKEO} target="_blank" rel="noreferrer">
                Política de privacidad
              </a>
            </label>
          </Checkbox>

          {getMsgError()}

          <StyledButton onClick={()=>handleSubmit()} >
            Enviar
          </StyledButton>
        </FormContent>
        :
        <div>
          { (props.sended === 'sended') ?
          <div>
            <Reveal trigger={<div />}>
              <Tween
                from={{ opacity: 0, transform: 'translate3d(0, 30vh, 0)' }}
                ease="back.out(1.4)"
                duration={1}
              >
                <SuccessImg>
                  <img style={{position: 'absolute'}} src={PAGE6.SuccessBckg} alt="success"/>
                </SuccessImg>
              </Tween>
            </Reveal>
            <FormContent>
              <SpinerImg src={PAGE6.SuccessIcon} />
              <h3>Tu mensaje fué enviado</h3>
              <p>
                Gracias por escribirnos <br />
                contactaremos contigo lo antes posible.
              </p>
              <StyledButton onClick={()=>{window.scrollTo(0,0);window.location.reload();}}>
                Continuar
              </StyledButton>
            </FormContent>
          </div>:
          <SpinerDiv>
            <ReactLoading type={'spin'} className="spiner" color={'#FF4759'} height={'30px'} width={'30px'} />
          </SpinerDiv>
          }
        </div>
      }
      </Container>
    </div>
  );
};

const ErrorMsg = styled.p`
  text-align: center;
  color: red; //
`;

const SpinerImg = styled.img`
  margin: auto;
  width: 36px;
`;

const SpinerDiv = styled.div`
  margin: auto;
  width: 30px;
`;

const SuccessImg = styled.div`
  text-align: left;
  z-index: 1;
  img {
    width: 660px;
    margin-top: -56px;
  };
  @media ( max-width: 720px ) {
    //display: none;
    img {
      width: 99%;
    }
  };
`;

const StyledButton = styled.button`
  font-family: Poppins;
  font-weight: bold;
  height: 35px;
  border: none;
  min-width: 185px;
  width: 62%;
  margin: auto;
  background: #FF4759;
  border-radius: 3.6px;
  font-size: 0.8em;
  color: #FFFFFF;
  letter-spacing: -0.4px;
  text-align: center;
`;

const Label = styled.label`
  font-weight: 600;
  font-size: 0.7em;
  margin-top: 0.3em;
  margin-bottom: 0.8em;
  text-align: left;
`;


const Checkbox = styled.div`
  display: flex;
  font-size: 0.75em;
  align-items: center;
  justify-content: center;
  letter-spacing: -0.5px;
  text-align: center;
  margin-top: 1.5em;
  margin-bottom: 1.5em;
  border: ${ props => props.warning ? '1px solid red' : 'none'};
  border-radius: 5px;

  #radio-btn {
    z-index: 1;
    min-width: 15px;
    width: 15px;
    height: 15px;
    background-color: ${props => props.isChecked ? '#1dc0c0' : '#FFFFFF' };
    border-radius: 15px;
    border: 2px solid #1dc0c0;
  }
  #input-base {
    opacity: 0;
    z-index: 3;
    margin-left: -15px;
  };

  .policy-link, a, a:visited {
    letter-spacing: 0px;
    font-weight: 600;
    color: #1dc0c0 !important;
    text-decoration: underline;
  };

  label {
    font-size: 0.95em;
  };

`;

const TopBorder = styled.div`
  width: 660px;
  height: 9px;
  margin: auto;
  margin-bottom: -9px;
  background: #FF4759;
  border-radius: 9px 9px 0px 0px;
  z-index: 1;
  position: relative;

  @media (max-width: 720px) {
    width: 90%;
  };
`;

const Container = styled.div`
  background: #FFFFFF;
  box-shadow: 45px 45px 90px 0 rgba(0,0,0,0.15);
  border-radius: 9px 9px;
  width: 660px;
  height: 276px;
  margin: auto;
  padding: 4em 0 1em 0;
  @media (max-width: 720px) {
    width: 90%;
  };
`

const FormContent = styled.div`
  width: 55%;
  margin: auto;
  display: grid;
  grid-template-columns: 100%;
  position: relative;
  @media (max-width: 720px) {
    width: 86%;
  };
`;
