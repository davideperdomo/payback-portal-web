import React from 'react';
import styled from 'styled-components';

export default function Content(props) {
  return (
    <ContentDiv props>
      {props.component}
    </ContentDiv>
  )
}

const ContentDiv = styled.div`
  height: ${window.innerHeight};
  background-color: ${ props => props.background ? props.background : 'white'};
  overflow: hidden;
`;
