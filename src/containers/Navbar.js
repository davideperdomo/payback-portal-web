import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import MagneticButton from '../components/MagneticButton';
import { LogoKeo, Icons } from '../assets/images/';
import styled from 'styled-components';

export default function Navbar() {
  const [toggle, setToggle] = useState(false);
  const [scroll, setScroll] = useState(0);
  const [scrollingUp, setScrollingUp] = useState(false);
  let history = useHistory();

  useEffect( () => {
    const handleScroll = (e) => {
      const position = window.pageYOffset;
      const scrolltop = position < scroll && position !== 0;
      setScroll(position);
      setScrollingUp(scrolltop);
    };
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, [scroll]);

  return (
    <div>
      <NavbarContainer className="navbar" >
        <a className="logo" href="/">
          <img src={LogoKeo} alt="logo-keo"/>
        </a>
        <List>  
          <a className="item" href="/howworks">How KEO works</a>
          <a className="item" href="/faq">FAQ</a>
          <a className="item" href="/contact">Contact Us</a>
        </List>
        <MagneticButton onPress={() => history.push("/howworks")}>
          {"Download & Request Cash"}
        </MagneticButton>
      </NavbarContainer>
      {toggle ? 
        <MenuResponsive>
          <Span close icon={Icons.IconClose} onClick={() => setToggle(false)} ></Span>
          <div>
            <div className="list-res">  
              <a className="item" href="/howworks">How KEO works</a> <br/>
              <a className="item" href="/faq">FAQ</a> <br/>
              <a className="item" href="/contact">Contact Us</a>
            </div>
          </div>
        </MenuResponsive>
        :
        <NavbarResponsive scrollingUp={scrollingUp}>
          <a className="logo" href="/">
            <img src={LogoKeo} alt="logo-keo" />
          </a>
          <Span icon={Icons.IconBurger} onClick={() => setToggle(true)} ></Span>
        </NavbarResponsive>
      }
    </div>
  )
}

const Span = styled.span`
  background: url(${props => props.icon});
  width: 35px;
  height: 35px;
  margin: 18px;
  margin-left: 5px;
  background-size: contain;
  background-repeat: no-repeat;
  display: block;
`;

const MenuResponsive = styled.div`
  background-color: #ff4458;
  width: 100vw;
  height: 110vh;
  z-index: 5;
  position: fixed;
  top: 0;
  span {
    float: right;
  };
  .list-res {
    padding-top: 80px;
    text-align: center;
  };
  .list-res a {
    font-family: Poppins-Bold;
    text-decoration: none;
    font-size: 38px;
    width: 100%;
    color: #FFFFFF;
    letter-spacing: -0.5px;
  }
`;

const NavbarResponsive = styled.div`
  display: none;
  height: 70px;
  span {
    float: right;
  }
  .logo {
    padding: 10px;
    float: left;
  }
  img {
    width: 50px;
  }
  @media (max-width: 720px) {
    display: block;
    position: ${props => props.scrollingUp?"fixed":"inherit"};
    width: 100%;
    top: ${props => props.scrollingUp?"0":"inherit"};
    background-color: #FFFFFF;
    z-index: 5;
  };
`;

const NavbarContainer = styled.div`
  padding: 3em 3.5em 0.5em 3.5em;
  display: grid;
  background-color: #FFFFFF;
  z-index: 5;
  grid-template-columns: 20% 55% 25%;
  .logo {
    grid-column: 1;
    margin-top: -30px;
  }
  @media (max-width: 900px) {
    padding: 3em 1.5em 0.5em 2em;
    margin: 0 0.5em 0 0.5em;
    grid-template-columns: 15% 50% 15%;
  };
  @media (max-width: 800px) {
    padding: 3em 1em 0.5em 1em;
    margin: 0 0.5em 0 0.5em;
    grid-template-columns: 15% 52% 15%;
  };
  @media (max-width: 720px) {
    display: none;
  };
`;

const List = styled.div`
  grid-column: 2 / 3;
  display: flex;  
  .item {
    font-family: Poppins-Bold;
    font-size: 12px;
    color: #303643;
    letter-spacing: 0.5px;
    text-align: center;
    text-decoration: none;
    padding: 1em 2em;
  }
`;
