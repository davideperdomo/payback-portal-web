import React from 'react';
import styled from 'styled-components';
import { OTHERS } from '../assets/images-new'

export default function Modal(props){
  const {open, onClose} = props;
  
  return (
    <React.Fragment >
      { open ?
      <ModalContainer>
        <ModalContent >
          <CloseIcon onClick={() => onClose()}>
            <img src={OTHERS.IconClose} alt="close"/>
          </CloseIcon>
          {props.children}
        </ModalContent>
      </ModalContainer>
      : <>< />
    }
    </React.Fragment >
  )
};

const CloseIcon = styled.span`
  display: block;
  margin-left: 96%;
  width: 18px;
  margin-top: -10px;
`;

const ModalContent = styled.div`
  background: #000000;
  box-shadow:0 9px 20px 7px rgba(0,0,0,0.50);
  border-radius: 8px;
  opacity: 0.96;
  font-weight: normal;
  font-size: 11px;
  letter-spacing: 0;
  line-height: 16px;
  width: 60%;
  min-width: 100px;
  text-align: left;
  color: #010101;
  z-index: 5;
  margin-top: 8px;
  margin: auto;
  position: relative;
  margin-top: 6vw;
  padding: 2em 1em 1em 1em;
  @media (max-height: 420px) {
    margin-top: 1em;
  };
  @media (max-width: 720px) {
    margin: auto;
    position: relative;
    margin-top: 4vw;
    padding: 2em;
    width: 80%;
  };
`;

const ModalContainer = styled.div`
  background-color: rgb(0,0,0,0.5);
  position: fixed;
  width: 100vw;
  height: 100vh;
  right: 0;
  top: 0;
  z-index: 5;
`;
