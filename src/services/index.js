import axios from 'axios';
const URL_BASE = `https://us-central1-gc-k-gbl-internal-zendesk-dev.cloudfunctions.net/sendTicket/`;

export const sendContact = async (form) => {
  const response = await axios.post(URL_BASE, form)
    .then(res => {
       //console.log(res);
       //console.log(res.data);
       return res.data ? res.data : res;
     })
    .catch(err => {
      return {
        status: 500,
        response: err.data.error ?? 'error not defined'
      }
    });
   return response;
};
