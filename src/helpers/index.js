export const getMove = (da, A, max, isStatic = false) => {
  const fixed = A/2;
  const movement = isStatic ? 0 : parseInt((da-fixed)/A  * max);
  return (movement<210)?movement:210;
};
