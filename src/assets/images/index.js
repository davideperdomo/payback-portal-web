import HomeCell1 from './img-home-cell1.png';
import HomeCell2 from './img-home-cell-2.png';
import HomeCell2Res from './img-home-cell-2-res.png';
import HomeDeco1 from './img-home-deco-1.png';
import HomeDeco2 from './img-home-deco-2.png';
import HomeDeco3 from './img-home-deco-3.png';
import HomeDeco4 from './img-home-deco-4.png';
import HomeCont1 from './img-home-cont-1.png';
import HomeCont1Res from './img-home-cont-1-res.png';
import HomeCont2 from './img-home-cont-2.png';
import HomeFacts1 from './img-home-facts-1.png';
import HomeFacts2 from './img-home-facts-2.png';
import HomeKnow from './img-knowmore.png';
import HowCells from './img-how-cells.png';
import HowArrow1 from './img-arrow-1.png';
import HowArrow2 from './img-arrow-2.png';
import FaqDeco1 from './img-faq-deco1.png';
import FaqDeco2 from './img-faq-deco2.png';
import FaqDeco3 from './img-faq-deco3.png';
import FaqDeco4 from './img-faq-deco4.png';
import LogoKeo from './logoKeo.png';
import IconGstore from './img-icon-gstore.png';
import IconIOSstore from './img-icon-iosstore.png';
import ContactImg from './img-contact.png';
import Icons from './icons'
import Cursor from './cursor.png'
import CursorMed from './cursor_med.png'

export { 
  LogoKeo,
  HomeCell1, 
  HomeCell2, 
  HomeCell2Res,
  HomeDeco1,
  HomeDeco2,
  HomeDeco3,
  HomeDeco4,
  HomeCont1,
  HomeCont1Res,
  HomeCont2,
  HomeFacts1,
  HomeFacts2,
  HomeKnow,
  HowCells,
  ContactImg,
  HowArrow1,
  HowArrow2,
  FaqDeco1,
  FaqDeco2,
  FaqDeco3,
  FaqDeco4,
  IconGstore,
  IconIOSstore,
  Icons,
  Cursor,
  CursorMed
 };