import Oval1 from './home-container2-oval1.png';
import Oval2 from './home-container2-oval2.png';
import Oval3 from './home-container2-oval3.png';
import Path1 from './home-container2-path1.png';
import Path2 from './home-container2-path2.png';
import Path3 from './home-container2-path3.png';
import Person from './home-container2-person.png';


const Container2 = { 
  Oval1,
  Oval2,
  Oval3,
  Path1,
  Path2,
  Path3,
  Person
};

export default Container2;