import IconBurger from './icon-burger.png';
import IconClose from './icon-close.png';
import IconDownload from './icon-download.png';
import IconLightning from './icon-lightning.png';
import IconLink from './icon-link.png';
import IconArrowUp from './icon-arrow-up.png';
import IconArrowDown from './icon-arrow-down.png';
import IconDot from './icon-dot-grey.png';
import IconDotKeo from './icon-dot-keo.png';

const Icons = { 
  IconBurger,
  IconClose,
  IconDownload,
  IconLightning,
  IconLink,
  IconArrowUp,
  IconArrowDown,
  IconDot,
  IconDotKeo
};

export default Icons;