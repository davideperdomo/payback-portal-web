import Background from './home-container1-background.png';
import CellsCol1 from './home-container1-cellscol1.png';
import CellsCol2 from './home-container1-cellscol2.png';
import CellsCol3 from './home-container1-cellscol3.png';
import Oval1 from './home-container1-oval1.png';
import Oval2 from './home-container1-oval2.png';

const Container1 = { 
  Background,
  CellsCol1,
  CellsCol2,
  CellsCol3,
  Oval1,
  Oval2
};

export default Container1;