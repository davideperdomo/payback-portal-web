import Oval1 from './home-mobile1-oval1.png';
import Oval2 from './home-mobile1-oval2.png';
import Oval3 from './home-mobile1-oval3.png';
import Path1 from './home-mobile1-path1.png';
import Path2 from './home-mobile1-path2.png';
import Path3 from './home-mobile1-path3.png';
import Phone from './home-mobile1-phone.png';

const Mobile1 = { 
  Oval1,
  Oval2,
  Oval3,
  Path1,
  Path2,
  Path3,
  Phone
};

export default Mobile1;