import React from 'react';
export const faqinfo = [
	{
		title: "¿Quién es KEO?",
		content: "Es una empresa de tecnología enfocada en servicios financieros y tenemos como objetivo promover la inclusión financiera, brindando a todos, independiente de su historial crediticio, la posibilidad de acceder a productos financieros de forma fácil, rápida y segura a través de la tecnología.",
		home: true
	},
	{
		title: "Costos",
		content: "Los costos son: Comision de solicitud de Tarjeta: MXN 200 iva incluido, Tasa interes ordinaria 9,59% simple mensual, Interes moratorio 20%, Gastos de cobranza MXN 300, Disposicion de efectivo ATM MXN 46 iva incluido. ",
		home: true
	},
	{
		title: "Tiempo de pago",
		content: "Para pagar el préstamo tienes 3 cuotas mensuales a partir de la fecha de desembolso del préstamo.",
		home: true
	},
	{
		title: "Estado del préstamo",
		content: "Para revisar el estado de tu préstamo puedes iniciar sesión con tus datos e ingresar a la sección: “Mis préstamos”.",
		home: true
	},
	{
		title: "Máximo preaprobado",
		content: "Para saber el monto máximo que tienes preaprobado debes ingresar a la App y ver el monto que puedes solicitar. El monto máximo varía dependiendo de cada persona. ",
		home: true
	},
	{
		title: "¿Cómo pago mi préstamo?",
		content: <p>{"Para pagar tu préstamo desde la App escoges la forma de pago entre transferencia bancaria o realizar el pago en efectivo en alguno de los comercios que reciben los pagos:"}
			<br/>{" -  Banorte "}<br/>{" - Citibanamex"}<br/>{" - Santander"}<br/>{" - Banco Azteca"}<br/>{" - BBVA"}<br/>{" - Préstamo Express"}<br/>{" - Tiendas del Ahorro"}
			<br/>{" - Comercial mexicana"}<br/>{" - Extra"}<br/>{" - Bodega Aurrera"}<br/>{" - HEB"}<br/>{" - 7 Eleven"}<br/>{" - Calimax"}<br/>{" - SuperQ"}<br/>{" - Super Norte"}
			<br/>{" - Soriana"}<br/>{" - Walmart"}</p>,
		home: true
	},
	{
		title: "¿Cuánto tarda el desembolso?",
		content: "El desembolso de tu préstamo no debería tardar más de 5 minutos.",
		home: true
	},
	{
		title: "Puntos PAYBACK",
		content: "Acumulas 1 Punto PAYBACK por cada 50 MXN de gasto con La Tarjeta.",
		home: true
	}
]

export const steps = [
	{
		title: "Eres un usuario PAYBACK \n",
		spanName: "download",
		content: "que quiere cumplir sus \nantojos, comprar hoy y \npagar después."
	},
	{
		title: "Necesitas una Tarjeta",
		spanName: "link",
		content: " para \npoder hacer todas tus \ncompras sin importar si son \npresenciales o en línea."
	},
	{
		title: "Te encantaría acumular \nPuntos PAYBACK",
		spanName: "lightning",
		content: " por todas tus compras, en tus tiendas \nfavoritas, sin importar \ndonde estés..."
	},
]

export const benefits = [
	{
		title: "¿Y si tienes que salir?",
		content: <React.Fragment>Usa tu Tarjeta en toda la red de establecimientos <span>American Express</span>.</React.Fragment>
	},
	{
		title: "¿Cansado de limitar tus compras?",
		content: "Un préstamo que puedes solicitar y recibir directamente en una Tarjeta."
	},
	{
		title: "¿No quieres salir de casa?",
		content: "Haz todas tus compras en línea con La Tarjeta."
	},
	{
		title: "¿Hay algo más?",
		content: <React.Fragment>Gana <b>Puntos PAYBACK</b> por todas las compras que hagas.</React.Fragment>
	}
]

export const faqinfolong = [
	{
		title: "1.	¿Qué es PAYBACK? ",
		content: "PAYBACK es el Monedero con el que puedes acumular Puntos PAYBACK al comprar en un montón de marcas participantes. Los Puntos que acumules sirven para pagar tus siguientes compras. De esta manera podrás cargar gasolina, ir al súper, pagar tu boleto de cine, de avión o disfrutar de una estancia increíble usando solo tus Puntos PAYBACK para pagar."
	},
	{
		title: "2.	¿Qué beneficios obtengo con PAYBACK? ",
		content: "Al acumular Puntos y utilizarlos en las marcas participantes, ahorras en tus compras diarias. Además, con PAYBACK recibirás promociones y cupones con los que podrás acumular Puntos extras, obtener descuentos y participar en concursos para ganar premios."
	},
	{
		title: "3.	¿Si tengo más de un Monedero PAYBACK puedo enlazarlos en un solo Monedero? ",
		content: <p>{"Sí. Solo comunícate al Centro de Atención Telefónica PAYBACK. 5553262538 (Ciudad de México) 8008383570 (interior de la República) De lunes a domingo de 9:00 a.m. a 8:00 p.m"}</p>

	},{
		title: "4.	¿Qué es KEO?",
		content: "Somos una empresa de tecnología enfocada en servicios financieros y tenemos como objetivo promover la inclusión financiera, brindando a todos, independiente de su historial crediticio, la posibilidad de acceder a productos financieros de forma fácil, rápida y segura a través de la tecnología. "
	},{
		title: "5.	¿Qué es mi préstamo KEO?",
		content: "Por ser uno de los clientes PAYBACK que recibieron un código de Acceso KEO, tendrás como beneficio el acceso a un préstamo que ya tienes preaprobado con el cual buscamos ayudarte en tu día a día. El préstamo lo podrás solicitar en cualquier momento y usarlo a tu conveniencia. Una vez hayas completado la solicitud, el dinero será depositado directamente en Tu Tarjeta KEO PAYBACK American Express para que lo puedas usar. Si es la primera vez que haces una solicitud, te enviaremos La Tarjeta a la dirección que nos brindes, cuando actives Tu Tarjeta recibirás el desembolso.Tu préstamo KEO se debe pagar en tres meses, si pagas el 100% de tu préstamo en el primer mes, no te cobramos intereses. Una vez pagues el 100% de tu préstamo, puedes solicitar sin comisión un nuevo préstamo."
	},{
		title: "6.	¿Qué es la Tarjeta KEO PAYBACK American Express?",
		content: "Es Una Tarjeta en la cual recibes tu préstamo KEO y puedes usar en toda la red de establecimientos American Express, realizar compras digitales, puedes hacer retiros en efectivo y todas las compras que hagas generan Puntos PAYBACK. Todos los préstamos que solicites serán desembolsados en tu tarjeta KEO Payback American Express."
	},{
		title: "7.	¿Qué es la App KEO préstamo?",
		content: <div>{"Es la APP donde te registras y puedes solicitar tu préstamo, solicitar Tu Tarjeta, manejar tus préstamos, ver el historial de transaccionales, bloquear Tu Tarjeta y hacer una nueva solicitud."}<br/>
				{"Para descargar en App Store haz clic "}<a href="https://apps.apple.com/us/app/keo-pr%C3%A9stamo/id1541684884" >acá.</a><br/>
				{"Para descargar en Play Store haz clic "}<a href="https://play.google.com/store/apps/details?id=com.keopayback.app">acá.</a></div>
	},{
		title: "8.	¿Cómo funciona el préstamo KEO?",
		content: <p>
		Si recibiste alguna comunicación indicándote que puedes acceder a nuestro préstamo y recibiste tu código de Acceso KEO ¡Eres uno de los elegidos y puedes solicitar este beneficio! ingresa a la APP KEO Préstamo y crea tu cuenta.
		Luego de crear tu cuenta, inicia sesión para poder ver el monto preaprobado que tenemos para ti; podrás solicitar el valor que quieres que te prestemos. Después, ingresa y confirma unos datos, acepta los Términos y Condiciones y disfruta de este beneficio. <br/>
		¿Qué esperas para descubrir cuánto te podemos prestar? Crea tu cuenta <a href="/">aquí</a><br/>
		El préstamo tendrá una duración de 3 meses y se puede pagar por transferencia bancaria o en efectivo en las siguientees sucursales** <br/>
		Si pagas el 100% de tu préstamo en el primer mes, no te cobramos intereses. Una vez pagues el 100% de tu préstamo, puedes solicitar sin comisión un nuevo préstamo, sólo puedes tener un préstamo activo a la vez.
		</p>
	},{
		title: "9.	¿Cómo me registro?",
		content: "Descarga la App y haz clic en registrarte, sigue los pasos. ¡Es muy fácil!"
	},{
		title: "10.	¿Qué significa tener un préstamo preaprobado?",
		content: "Significa que cuando te registras en la plataforma, nuestra herramienta de análisis crediticio revisa en tiempo real tu perfil (sin tener en cuenta tu historial crediticio) y así, al iniciar sesión con tus datos, podrás ver el valor máximo de dinero que puedes obtener como préstamo."
	},{
		title: "11.	¿Cuáles son las condiciones del préstamo?",
		content: "El préstamo lo solicitas desde la App KEO Préstamo y será desembolsado en Tu Tarjeta KEO PAYBACK American Express cuando ya este activada. Así, lo podrás empezar a disfrutar de manera inmediata."+
			"El préstamo tiene un plan de pagos para tres cuotas mensuales. Tiene un costo de 9,59% interés mensual y si en el primer mes pagas a tiempo tu cuota no pagas intereses. Sin embargo, si te atrasas en el pago el costo de intereses moratorios es de 20% Mensual."
	},{
		title: "12.	¿Cómo solicito un préstamo de KEO?",
		content: <p>{"¡Es muy fácil! Una vez te hayas registrado, sigue los siguientes pasos:"}<br/>
{"1.	Ingresa a la APP e inicia sesión."}<br/>
{"2.	Indica la cantidad de dinero que deseas solicitar escribiendo el monto o eligiendo la cantidad en los botones inferiores."}<br/>
{"3.	Revisa el resumen de tu préstamo:  Monto solicitado, número de cuotas, costo de plataforma, interés y fecha de tu primer descuento."}<br/>
{"4.	Acepta los Términos y Condiciones y la Política de cobranza."}<br/>
{"5.	Finalmente, confirma la solicitud haciendo clic en “Continuar”."}<br/>
{"6.	Validaremos la información suministrada y te enviaremos un correo al finalizar la validación. Recuerda, para recibir tu préstamo también debes hacer la solicitud de Tu Tarjeta en la App KEO Préstamo."}
</p>
	},{
		title: "13.	¿Cómo se la cantidad mínima y máxima que puedo solicitar?",
		content: "Una vez entres a la App e inicies sesión, podrás ver la cantidad máxima o mínima que puedes solicitar dando clic en la opción “solicitar”."
	},{
		title: "14.	¿Cuál es la tasa de interés y los costos adicionales?",
		content: "Comisión de apertura MXN 200 IVA incluido que soló pagas una vez, Tasa interés ordinaria 9,59% simple mensual, Interes moratorio 20%, Gastos de cobranza MXN 300, Disposicion de efectivo ATM MXN 46 iva incluido."
	},{
		title: "15.	¿Cuánto se demora la aprobación y desembolso del préstamo?",
		content: "El desembolso de tu préstamo no debería tardar más de 5 minutos desde el momento de la solicitud. (Cuando ya tienes Tu Tarjeta activada)."
	},{
		title: "16.	¿Dónde me abonan el préstamo?",
		content: "El préstamo se te carga en Tu Tarjeta KEO PAYBACK American Express para que lo puedas usar en tus compras digitales o físicas."
	},{
		title: "17.	¿Cómo pago el préstamo?",
		content: "El préstamo lo puedes pagar por transferencia bancaria o por efectivo. En este enlace puedes consultar todos los establecimientos donde puedes pagar."
	},{
		title: "18.	¿Cómo puedo consultar la fecha de pago y la cantidad a pagar de mi préstamo?",
		content: "Inicia sesión en nuestra App KEO Préstamo. En la sección “Inicio” podrás encontrar un botón “Ver detalle del préstamo” da clic a esta opción, allí encontrarás el monto pendiente de pago de tu préstamo, una lista de tus pagos realizados y el detalle de cada uno."
	},{
		title: "19.	¿Puedo retirar en efectivo mi préstamo?",
		content: "Ya que el dinero es depositado en Tu Tarjeta KEO PAYBACK, el préstamo lo puedes usar para hacer retiros de efectivo en cualquier cajero Scotiabank o HSBC o compras físicas o en línea."
	},{
		title: "20.	¿Cómo usar mi Tarjeta KEO PAYBACK American Express?",
		content: "Puedes usar tu Tarjeta KEO PAYBACK American Express en toda la red de establecimientos American Express, en compras digitales y hacer retiros en efectivo en HSCB y Banco Santander."
	},{
		title: "21.	¿Cómo solicito mi Tarjeta KEO PAYBACK American Express?",
		content: "Para solicitar Tu Tarjeta debes ingresar a la App, allí encontraras la opción de solicitud de tarjeta después que hayas realizado una solicitud de préstamo, sigue el paso a paso y ¡listo! Para recibir tu Tarjeta debes pagar 200 MXN para que sea emitida. (Este cobro se realiza una sola vez)."
	},{
		title: "22.	¿Mi tarjeta tiene algún costo? ",
		content: "La tarjeta tiene un costo único de 200 MXN que pagas solo la primera vez que solicitas la Tarjeta. Por cada retiro en efectivo que hagas deberás pagar MXN 46 IVA incluido, los retiros los puedes realizar en cuaquier cajero Scotiabank o HSBC."
	},{
		title: "23.	¿Dónde puedo usar mi Tarjeta KEO PAYBACK American Express?",
		content: "Puedes usar tu Tarjeta KEO PAYBACK American Express en toda la red de establecimientos American Express y hacer retiros en efectivo en Scotiabank o HSBC."
	},{
		title: "24.	¿Recibo algo extra por usar mi Tarjeta?",
		content: "Recibes Puntos PAYBACK por todas las compras que realices en comercios con Tu Tarjeta."
	},{
		title: "25.	¿Recibo Puntos PAYBACK en todos los comercios?",
		content: "Recibes Puntos PAYBACK por realizar compras en todos los comercios donde utilices tu Tarjeta sin importar si son nacionales o internacionales."
	},{
		title: "26.	¿Qué pasa si pierdo mi Tarjeta KEO PAYBACK American Express?",
		content: <p>{"1.	Debes bloquear temporalmente o cancelar Tu Tarjeta desde la App."}<br/>{"2.	hay una opción de “olvide mi contraseña”, haz clic y sigue los pasos para crear una nueva."}</p>
	},{
		title: "27.	¿Cuántos Puntos PAYBACK recibo por cada transacción?",
		content: "Por cada transacción que hagas recibes el 0,25% de la transacción en Puntos PAYBACK."
	},{
		title: "28.	¿Puedo bloquear temporalmente mi Tarjeta KEO PAYBACK American Express?",
		content: "Puedes bloquear temporalmente o cancelar Tu Tarjeta desde la App."
	},{
		title: "29.	¿Qué pasa si olvidé mi contraseña?",
		content: "En la App hay una opción de “olvide mi contraseña”, haz clic y sigue los pasos para crear una nueva."
	},{
		title: "30.	¿Puedo invitar a alguien a que solicite su Tarjeta? ",
		content: ""
	},{
		title: "31.	¿Qué es mi código de Acceso KEO?",
		content: "Es un código único que reciben los usuarios PAYBACK que pueden acceder a la Tarjeta KEO PAYBACK American Express y viene en los e-mails que reciben de PAYBACK. Ese código único es el que necesitan para solicitar su tarjeta."
	},{
		title: "32.	¿Dónde puedo consultar los Términos y Condiciones de mi préstamo?",
		content: "Puedes consultar los términos y condiciones del préstamo KEO justo antes de solicitar un préstamo haciendo clic en “Términos y Condiciones”. Adicionalmente, serán enviados a tu correo con la confirmación del desembolso exitoso de tu préstamo o haciendo clic AQUÍ."
	},{
		title: "33.	¿Existen multas o penalidades por pagos fuera del tiempo determinado?",
		content: "Al no pagar tu préstamo a tiempo incurrirás en costos de intereses moratorios por 20% MV."
	},{
		title: "34.	¿Puedo pedir un préstamo nuevo sin acabar de pagar el anterior?",
		content: "Tienes que terminar de pagar el préstamo que tienes activo para poder solicitar uno nuevo."
	},{
		title: "35.	¿Cuál es mi plazo para pagar el préstamo KEO?",
		content: "Tienes 3 cuotas mensuales para pagar tu préstamo."
	},{
		title: "36.	¿Puedo modificar/incrementar el plazo para pagar mi préstamo KEO?",
		content: "El plazo de tu préstamo será siempre 3 meses."
	}
]
