import FaqDeco from "./FAQ_DECO_1.png";
import FooterIconFacebook from "./facebook-btn.png";
import FooterIconInstagram from "./instagram-btn.png";
import LogoKeoPayback2 from "./LOGO-KEOPAYBACK-2.png";
import IconGstore from "./STORE-GOOGLE.png"
import IconIOSstore from "./STORE-APPLE.png"
import IconArrowUp from './ICON_ARROW_UP.png'
import IconArrowDown from './ICON_ARROW_DOWN.png'
import IconClose from './ICON_CLOSE.png'

const OTHERS = {
  FaqDeco,
  FooterIconFacebook,
  FooterIconInstagram,
  LogoKeoPayback2,
  IconGstore,
  IconIOSstore,
  IconArrowUp,
  IconArrowDown,
  IconClose
};

export default OTHERS;
