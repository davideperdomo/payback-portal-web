import CardBckg from "./PAGE1_CARD_BCK.png";
import Card from "./PAGE1_CARD.png";
import Title from "./PAGE1_TITLE.png";

const PAGE1 ={
  Card,
  CardBckg,
  Title
};

export default PAGE1;
