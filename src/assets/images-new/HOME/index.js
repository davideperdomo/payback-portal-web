//import CarouselImg1 from './HOME_CAR_1.png';
//import CarouselImg2 from './HOME_CAR_2.png';
//import CarouselImg3 from './HOME_CAR_3.png';
import CarouselImg1 from './HOME_PERSON_1.png';
import CarouselImg2 from './HOME_PERSON_2.png';
import CarouselImg3 from './HOME_PERSON_3.png';
import CarouselBckg1 from './HOME_BCKG_1.png';
import CarouselBckg2 from './HOME_BCKG_2.png';
import CarouselBckg3 from './HOME_BCKG_3.png';
import DecoDots from "./HOME_DECO_DOTS.png";
import LogoKeoPayback from "./HOME_LOGO.png";
import IconPlay from "./HOME_ICON_PLAY.png";
import ArrowDown from "./HOME_ARROW_DOWN.png"

const HOME = {
  CarouselImg1,
  CarouselImg2,
  CarouselImg3,
  CarouselBckg1,
  CarouselBckg2,
  CarouselBckg3,
  DecoDots,
  LogoKeoPayback,
  IconPlay,
  ArrowDown
 };

 export default HOME;
