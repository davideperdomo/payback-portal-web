import HOME from "./HOME";
import PAGE1 from "./PAGE1";
import PAGE2 from "./PAGE2";
import PAGE3 from "./PAGE3";
import PAGE4 from "./PAGE4";
import PAGE5 from "./PAGE5";
import SuccessBckg from "./PAGE6/PAGE6_SUCCESS_BCKG.png"
import SuccessIcon from "./PAGE6/PAGE6_SUCCESS_ICON.png";
import OTHERS from "./OTHERS";

const PAGE6 = { SuccessBckg, SuccessIcon };

export {
  HOME,
  PAGE1,
  PAGE2,
  PAGE3,
  PAGE4,
  PAGE5,
  PAGE6,
  OTHERS
};
