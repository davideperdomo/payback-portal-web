import IconBox1 from "./PAGE2_BOX_1.png";
import IconBox2 from "./PAGE2_BOX_2.png";
import IconBox3 from "./PAGE2_BOX_3.png";
import Deco1 from "./PAGE2_DECO_1.png";

const PAGE2 = {
  IconBox1,
  IconBox2,
  IconBox3,
  Deco1
};

export default PAGE2; 
