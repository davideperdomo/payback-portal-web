import Arrow1 from "./PAGE3_ARROW_1.png";
import Arrow2 from "./PAGE3_ARROW_2.png";
import Arrow3 from "./PAGE3_ARROW_3.png";
import Arrow4 from "./PAGE3_ARROW_4.png";
import OvalBckg from "./PAGE3_OVAL_BCKG.png";
import Card from "./PAGE3_CARD.png";
import Phone from "./PAGE3_PHONE.png";
import Deco1 from "./PAGE3_DECO_1.png";
import Dots from "./PAGE3_DOTS.png";

const PAGE3 = {
  Arrow1,
  Arrow2,
  Arrow3,
  Arrow4,
  OvalBckg,
  Card,
  Phone,
  Deco1,
  Dots
};

export default PAGE3;
