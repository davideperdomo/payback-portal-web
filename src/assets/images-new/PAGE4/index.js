import CarouselArrowLeft from "./PAGE4_CAR1_ARROWLEFT.png";
import CarouselArrowRight from "./PAGE4_CAR1_ARROWRIGHT.png";
import Carousel1Bckg from "./PAGE4_CAR1_BCKG.png";
import Carousel1Phone from "./PAGE4_CAR1_PHONE.png";
import Carousel2Bckg from "./PAGE4_CAR2_BCKG.png";
import Carousel2Phone from "./PAGE4_CAR2_PHONE.png";
import Carousel3Bckg from "./PAGE4_CAR3_BCKG.png";
import Carousel3Phone from "./PAGE4_CAR3_PHONE.png";
import Carousel4Phone from "./PAGE4_CAR4_PHONE.png";
import Carousel4Card from "./PAGE4_CAR4_CARD.png";
import Carousel4Bckg from "./PAGE4_CAR4_BCKG.png";

const PAGE4 = {
  CarouselArrowLeft,
  CarouselArrowRight,
  Carousel1Bckg,
  Carousel1Phone,
  Carousel2Bckg,
  Carousel2Phone,
  Carousel3Bckg,
  Carousel3Phone,
  Carousel4Card,
  Carousel4Phone,
  Carousel4Bckg
};

export default PAGE4;
