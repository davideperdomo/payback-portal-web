import React, { useEffect } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'; 
import Home from './pages/Home';
import Faq from './pages/Faq';
import sorteopdf from './assets/T&C_sorteo_payback.pdf'

const Terms = () => {
  useEffect( ()=>{
    window.location.assign(sorteopdf)
  } ,[])
  return <></>
}

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/payback/faq' component={Faq} />
        <Route exact path='/payback/tyc-sorteo' component={Terms} />
        <Route path='/payback/' component={Home} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
